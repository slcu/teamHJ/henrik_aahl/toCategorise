import math
import cPickle

dataPath = "/home/Niklas/Desktop/lisa/7Oct2013/"
destPath = "/home/lisa/Desktop/data_convert_newman/"
organism = "organism/"
timePlantHminInput = open(destPath + "timePlantHMin.txt", "r")
tag = ".txt"
numVars = 4; #cell volume, PIN conc in cell, PIN conc on anticlinal walls, PIN conc on periclinal walls

for tph in timePlantHminInput.readlines():
#read in time, plant, h_min data and load in files accordingly
    tphStr = tph.split(" ")
    while "" in tphStr: tphStr.remove("")
    timeFolder = tphStr[0] + "hrs/"
    PMfilename =  tphStr[0] + "hrs_plant_"+ tphStr[1]+"-acylYFP_hmin_"+str(int(tphStr[2]))+"_clean_3_bk_ok_top_organism_init"
    label2ExpressionValuesFile = dataPath + timeFolder + "label2ExpressionValues/" +  "label2ExpressionValue_"+tphStr[0]+"hrs_plant_"+tphStr[1]+"-acylYFP_hmin_"+str(int(tphStr[2]))+"_clean_3_bk_ok_walls_Width_2_erosion_1_surfaceDil_2.pkl"
    PMinput = open(dataPath + timeFolder + organism + PMfilename + tag,"r")
    PMlines = PMinput.readlines()
    PMinput.close()
#depickle files
    fobj = file(label2ExpressionValuesFile)
    [label2ExpressionValuesOnCellWall, cellBorderVoxelNb, label2ExpressionValues, cellVoxelNb, label2ExpressionValuesOnAnticlinalWalls, anticlinalVoxelNb, label2ExpressionValuesOnPericlinalWalls, periclinalVoxelNb, label2ExpressionValuesOnPericlinalWallsExcludingSurface, periclinalVoxelNbExcludingSurface] = cPickle.load(fobj)
    fobj.close()

#initialise output file.  NB. time folders must exist in correct directory !
    output = open(destPath + timeFolder + PMfilename + "_PIN_newman" + tag,"w")
    s = PMlines[0].split(" ");
    output.write("1\n")
    output.write("%s %i %i\n" % (s[0], 4 + numVars, 0))


    for i in range(3,len(PMlines)-1):
        l = PMlines[i]
        s = l.split(" ")
        while "" in s: s.remove("")
        rad = float(s[3])
        rad = math.pow(3 * rad / (4. * math.pi),1./3.)
        if(rad > 5): print(str(tph) + str(rad) + " " + str(i) + " " + str(l))
       # else:
 #total cell
        nb = float(cellVoxelNb[i+1])
        if nb == 0: cell = 0
        else: cell = float(label2ExpressionValues[i+1])/nb
    #anticlinal walls
        nb = float(anticlinalVoxelNb[i+1])
        if nb == 0: antiwall = 0
        else: antiwall = float(label2ExpressionValuesOnAnticlinalWalls[i+1])/nb
    #periclinal walls
        nb = float(periclinalVoxelNb[i+1])
        if nb == 0: periwall = 0
        else: periwall = float(label2ExpressionValuesOnPericlinalWalls[i+1])/nb
        output.write("%s %s %s %f %s %f %f %f\n" % (s[0],s[1],s[2],rad,s[3],cell,antiwall,periwall))

    output.close()

timePlantHminInput.close()
