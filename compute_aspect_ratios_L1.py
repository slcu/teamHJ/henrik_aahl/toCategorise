import math
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pylab
from scipy.stats.stats import pearsonr
from mpl_toolkits.mplot3d import Axes3D

dataPath = "/home/lisa/Desktop/aspect_ratios_L1_central_zones/"
filenames = [
"0hrs_plant_8_2013_10_07_L1_aspectRatios.txt",
"0hrs_plant_13_2013_10_07_L1_aspectRatios.txt",
"PlantA_zoomIn_3_2014_10_25_L1_aspectRatios.txt",
"PlantB_2hrs_2013_03_14_L1_aspectRatios.txt",
"PlantB_zoomIn_3_2014_10_25_L1_aspectRatio.txt",
"plant7_0hrs_2014_10_07_L1_aspectRatios.txt",
"plant8_0hrs_2014_10_07_L1_aspectRatio.txt",
"plant1_4hrs_2014_09_05_L1_aspectRatios.txt",
"plant3_0hrs_2014_09_05_L1_aspectRatios.txt",
"plant5_0hrs_2014_09_05_L1_aspectRatios.txt",
"plant8_0hrs_2014_09_05_L1_aspectRatios.txt",
"plant9_0hrs_2014_09_05_L1_aspectRatios.txt",
"plant11_0hrs_2014_09_05_L1_aspectRatios.txt",
"plant12_4hrs_2014_09_05_L1_aspectRatios.txt"
]

scaleFactor = [
0.2635765,
0.2635765,
0.1660532,
0.2635765,
0.1660532,
0.2635765,
0.2635765,
0.2635765,
0.2635765,
0.2635765,
0.2635765,
0.2635765,
0.2635765,
0.2635765
]
#column 1  = index.  Each pair of rows (i, i+1), i = 0,2,4,6,.... The final entry contains the length (i) or width (i+1) of the same cell. 

longAxis = []
shortAxis = []
aspectRatio = []

for i in range(0, len(filenames)):
    dataFile = open(dataPath + filenames[i], "r")   
    data = dataFile.readlines()
    dataFile.close()

    for j in range(0,len(data)):
        d = data[j].split('\t')
        d[-1] = d[-1][:-1] #remove /n final character
        try:
            length = float(d[-1])
            if j%2 == 0: longAxis.append(length * scaleFactor[i])
            else: shortAxis.append(length * scaleFactor[i])            
        #dataArr.append(d)
        except ValueError:
            print  
    plt.plot(shortAxis, longAxis, 'ro')
    plt.xlabel('short axis (micron)')
    plt.ylabel('long axis (micron)')
    plt.title("central zone L1 cells")
    plt.savefig(dataPath + "width_vs_length_" + filenames[i] +".pdf", format='pdf')
    plt.close()

longAxisnew = []
shortAxisnew = []
for i in range(0, len(longAxis)):
    if(shortAxis[i] < longAxis[i]): 
        longAxisnew.append(longAxis[i])    
        shortAxisnew.append(shortAxis[i])
    else:
        shortAxisnew.append(longAxis[i])    
        longAxisnew.append(shortAxis[i])    

aspectRatio = [shortAxisnew[i]/longAxisnew[i] for i in range(0, len(longAxisnew))]
print "n = ", len(aspectRatio)

plt.hist(aspectRatio, bins = 10)
plt.xlabel('aspect ratio ')
plt.title("central zone L1 cells")
plt.savefig(dataPath + "hist_L1_aspectRatios.pdf", format='pdf')
plt.close()

plt.plot(longAxisnew, shortAxisnew, 'ro')
plt.xlabel('length of long axis (micron)')
plt.ylabel('length of short axis (micron)')
plt.title("central zone L1 cells")
plt.savefig(dataPath + "short_vs_long_axis_L1.pdf", format='pdf')
plt.close()

plt.plot([shortAxisnew[i] for i in range(0, len(longAxisnew))], aspectRatio, 'ro')
plt.xlabel('length of short axis (micron)')
plt.ylabel('aspect ratio')
plt.title("central zone L1 cells")
plt.savefig(dataPath + "short_axis_vs_aspectRatio_L1.pdf", format='pdf')
plt.close()
print "Pearson coeff. short axis vs aspectRatio", pearsonr(shortAxisnew, aspectRatio)

plt.plot([longAxisnew[i] for i in range(0, len(longAxisnew))], aspectRatio, 'ro')
plt.xlabel('length of long axis (micron)')
plt.ylabel('aspect ratio')
plt.title("central zone L1 cells")
plt.savefig(dataPath + "long_axis_vs_aspectRatio_L1.pdf", format='pdf')
plt.close()
print "Pearson coeff. long axis vs aspectRatio", pearsonr(longAxisnew, aspectRatio)


plt.plot([shortAxisnew[i]*longAxisnew[i] for i in range(0, len(longAxisnew))], aspectRatio, 'ro')
plt.xlabel('area (micron^2)')
plt.ylabel('aspect ratio')
plt.title("central zone L1 cells")
plt.savefig(dataPath + "volume_vs_aspectRatio_L1.pdf", format='pdf')
plt.close()
print "volume vs aspectRatio", pearsonr([shortAxisnew[i]*longAxisnew[i] for i in range(0, len(longAxisnew))], aspectRatio)


fig = pylab.figure()
ax = Axes3D(fig)
ax.scatter(shortAxisnew, longAxisnew, aspectRatio)
plt.savefig(dataPath + "3D.pdf", format='pdf')
plt.close()

