from openalea.image.spatial_image import SpatialImage
from scipy.ndimage.filters import gaussian_filter
from openalea.image.all import imread, imsave
import matplotlib.pyplot as plt
import numpy as np
from scipy import ndimage
import scipy.ndimage as nd


sliceRange = (1, 146)

def applyPalette(im, base_dict, threshold=None, dec=1000):
    outliers=set(np.array(base_dict.keys())[np.array(base_dict.values())>threshold])
    palette=np.zeros(np.max(im)+1, dtype=np.uint16)
    for i in outliers:
        palette[i]=base_dict[i]*dec
    return palette[im] 

def concatLabels(im):
    if (set(np.unique(im))!=set(range(1, np.max(im)+1))):
        im[im==0]=im.max()+1
        histo=nd.histogram(im, min=0, max=np.max(im), bins=np.max(im)+1)
        if np.argsort(histo)[-1]!=1:
            im[im==1]=im.max()+1
            im[im==np.argsort(histo)[-1]]=1
        labels=np.unique(im)
        labels.sort()
        labels_con=np.linspace(1, len(labels), len(labels)).astype(np.uint16)
        mapping=dict(zip(labels, labels_con))
        return applyPalette(im, mapping, dec=1), zip(labels, labels_con)
    else:
        return im, None


segmentedImageFName = "/home/lisa/Desktop/segmentations_2014_10_07_PGMY_timecourse/0hrs/ALT_plant4_0_4hrs/plant4_slow_0hrs-acylYFP_ed_hmin_2_new_clean_3_borderCellsRemoved.tif"
segmentedImage = imread(segmentedImageFName)

correctedFileName = "/home/lisa/Desktop/segmentations_2014_10_07_PGMY_timecourse/0hrs/ALT_plant4_0_4hrs/plant4_0hrs_corrected.tif"

shape = segmentedImage.shape


def mergeImagesOld(imageNb):
#     newImage =  np.ones(shape = [segmentedImage.shape[0], segmentedImage.shape[1], imageNb], dtype = segmentedImage.dtype)
    
    for i in xrange(imageNb):
        print i
        newSlice = np.load("/home/lisa/Desktop/segmentations_2014_10_07_PGMY_timecourse/0hrs/ALT_plant4_0_4hrs/corrected_image/corrected_%d.tif.npy"%( -i))
        print np.unique(newSlice)
#         print np.where(newSlice >= 30000)
#         newSlice[newSlice >= 30000] = 30000
#         raw_input()
        segmentedImage[...,  - i] = newSlice
#         print np.unique(segmentedImage[..., shape[2] - i]) 
#         raw_input()
    imsave(correctedFileName, SpatialImage(segmentedImage[... , shape[2] - imageNb : shape[2] ]))
    
    
    
def mergeImages(maxLabel = 40000):
#     for i in xrange(shape[2]):
    for i in xrange(sliceRange[1], sliceRange[0] - 1, -1):
        print i
        newSlice = np.load("/home/lisa/Desktop/segmentations_2014_10_07_PGMY_timecourse/0hrs/ALT_plant4_0_4hrs/corrected_image/corrected_%d.tif.npy"%i)
        labels = np.unique(newSlice)
        labels = labels[labels >= 30000]
        print labels
        for lab in labels:
            newSlice[newSlice == lab] = maxLabel
            maxLabel += 1
        segmentedImage[..., i] = newSlice
#     raw_input()
    imsave(correctedFileName, SpatialImage(segmentedImage[... , sliceRange[0] : sliceRange[1] ]))
    print maxLabel

            
def mergeCellsOnSlices(image):
    for i in xrange(image.shape[2] - 1, 0, -1):
        print i, 50 * "="
        j = i - 1
        currentSlice = image[..., i]
        nextSlice = image[..., j]
        currentLabels = np.unique(currentSlice)
        currentLabels = currentLabels[currentLabels > 30000]
        print "current labels: ", currentLabels
        for cl in currentLabels:
            nextLabels = np.unique(nextSlice)
            nextLabels = nextLabels[nextLabels > 30000]
            print "next labels: ", nextLabels
            print "cl = ", cl
            w = np.where(currentSlice == cl)
            cA = set(zip(*[w[0], w[1]]))
            for nl in nextLabels:
                print "nl = ", nl
                w2 = np.where(nextSlice == nl)
                nA = set(zip(*[w2[0], w2[1]]))
                intersect = cA.intersection(nA)
                print len(intersect), len(nA), len(cA)
                r1, r2 = len(intersect) / float(len(nA)), len(intersect) / float(len(cA))
                if (r1 > 0.5) or (r2 > 0.5):
                    nextSlice[nextSlice == nl] = cl
                    print nl, " replaced"
    return image
                
        
#         print np.unique(nextSlice)
        
        
def exploreImageOld(image):
    labels = np.unique(image)
    addedLabels = labels[labels > 30000]
    print len(addedLabels)
    for lab in addedLabels:
        coord = np.where(image == lab)
#         print np.max(coord[2]), np.min(coord[2])
        currentSlice = image[..., np.min(coord[2])]
        w = np.where(currentSlice == lab)
        cA = set(zip(*[w[0], w[1]]))
        intersectList = []
        maxIntersectLab = -1
        maxIntersect = -1
        if np.min(coord[2]) > 0:
            nextSlice = image[..., np.min(coord[2]) - 1]
            nextLabels = np.unique(nextSlice)
            for nl in nextLabels:          
                w2 = np.where(nextSlice == nl)
                nA = set(zip(*[w2[0], w2[1]]))
                intersect = cA.intersection(nA)
                intersectList.append(len(intersect))
                if (len(intersect) > maxIntersect ) and (nl != 1):
                    maxIntersect = len(intersect)
                    maxIntersectLab = nl
#                 if len(intersect) > 0:
#                     print lab, nl, "(", len(intersect), ")", "--",
#             intersectList.sort(reverse = True)
#             print intersectList
            print "here : ", maxIntersect, maxIntersectLab, lab
            raw_input()
            if maxIntersectLab != -1:
                image[coord] = maxIntersectLab
    return image
        
        
def exploreImage(image):
    labels = np.unique(image)
    addedLabels = labels[labels > 30000]
    print len(addedLabels)
    for lab in addedLabels:
        coord = np.where(image == lab)
#         print np.max(coord[2]), np.min(coord[2])
        currentSlice = image[..., np.min(coord[2])]
        w = np.where(currentSlice == lab)
        cA = set(zip(*[w[0], w[1]]))
        intersectList = []
        maxIntersectLab = -1
        maxIntersect = -1
        if np.min(coord[2]) > 0:
            nextSlice = image[..., np.min(coord[2]) - 1]
            nextLabels = np.unique(nextSlice)
            for nl in nextLabels:          
                w2 = np.where(nextSlice == nl)
                nA = set(zip(*[w2[0], w2[1]]))
                intersect = cA.intersection(nA)
                intersectList.append(len(intersect))
                if (len(intersect) > maxIntersect ) and (nl != 1):
                    maxIntersect = len(intersect)
                    maxIntersectLab = nl
#                 if len(intersect) > 0:
#                     print lab, nl, "(", len(intersect), ")", "--",
#             intersectList.sort(reverse = True)
#             print intersectList
            print "here : ", maxIntersect, maxIntersectLab, lab
            raw_input()
            if maxIntersectLab != -1:
                image[coord] = maxIntersectLab
    return image
        



    
if __name__ == "__main__":
    mergeImages()
    image = imread(correctedFileName)
    image = mergeCellsOnSlices(image)
    print len(np.unique(image))
     
    imsave(correctedFileName, image)
    

    
    newImage = imread(correctedFileName)
    print image.shape
#     newImage = exploreImage(newImage)
    newImage = ndimage.rotate(newImage, -90)
    newImage, d = concatLabels(newImage)
    imsave(correctedFileName, SpatialImage(newImage))
    
    
    
    
    
    

    
    
