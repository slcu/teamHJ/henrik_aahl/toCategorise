from openalea.image.algo.graph_from_image import SpatialImageAnalysis
from openalea.image.all import imread, imsave
import numpy as np
import matplotlib.pyplot as plt


fileName = "/media/Seagate Expansion Drive/storage/data/YassinRay/110614_myPG/lsm/plantA/segmented/C1-PlantA_128hrs_slicesOk_8bits_hmin_4.tif"
cellLabelsToRemove = []


tissueImage = imread(fileName)
background = 1

print "number of cells to remove = ", len(cellLabelsToRemove)
for i in cellLabelsToRemove:
    print i
    np.putmask(tissueImage, tissueImage == i, background)
newFileName = fileName[:-4] + "_cellRemoved.tif"
imsave(newFileName, tissueImage)
     
