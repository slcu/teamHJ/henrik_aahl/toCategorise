from scipy.spatial.distance import cdist
from scipy.spatial import KDTree
import numpy as np
import cPickle


def mergeClosePoints(points, cells, distanceThreshold):
    dist = cdist(points, points)
#     for i in xrange(len(dist.shape[0])):
    indexes = np.where(dist < distanceThreshold)
    toMerge = {i : set() for i in indexes[0]}
    for i in xrange(len(indexes[0])):
        if indexes[0][i] != indexes[1][i]:
            toMerge[indexes[0][i]].add(indexes[1][i])
    toMerge = dict((item, toMerge[item]) for item in toMerge if len(toMerge[item]) > 0)    
    newToMerge = dict()
    checkedItems = set()
    for item, s in toMerge.iteritems():
        if item not in checkedItems:
            for item2 in s:
                if item2 not in checkedItems:
                    if len(s) >= len(toMerge[item2]):
                        newToMerge[item] = s
                    else:
                        newToMerge[item] = toMerge[item2]
        checkedItems.add(item)
        checkedItems.update(s)
        
    
    return toMerge, newToMerge
    


def indexesToRemove(points, distanceThreshold):
    kdt = KDTree(points)
    pairs = kdt.query_pairs(distanceThreshold)
    d = dict((p[0], set()) for p in pairs)
    d2 = dict((p[1], set()) for p in pairs)
    d.update(d2)
    for p in pairs:
        d[p[0]].add(p[1])
        d[p[1]].add(p[0])
    maxLen = -1
    vertexIndex = -1
    for i, s in d.iteritems():
        if maxLen < len(s):
            maxLen = len(s)
            vertexIndex = i
    if vertexIndex != -1:
        toRemove = set(d[vertexIndex])
    else:
        toRemove = set()
    
    return vertexIndex, toRemove
    
def merge(points, cells, distanceThreshold):
    originalPoints = np.array(points)
    counter = 0
    toMerge = []
    while True:
        vertexIndex, toRemove = indexesToRemove(points, distanceThreshold)
        if len(toRemove) != 0:
            toMerge.append([points[vertexIndex], [points[k] for k in toRemove]])
        points = [points[i] for i in xrange(len(points)) if i not in toRemove]
        if vertexIndex == -1:
            break
        counter += 1
    for item in toMerge:
        p = item[0]
        toreplace = item[1]
        for tr in toreplace:
            for i in xrange(len(originalPoints)):
                if ( np.abs(originalPoints[i][0] - tr[0]) < 0.001 ) and ( np.abs(originalPoints[i][1] - tr[1]) < 0.001 ):
                    originalPoints[i] = p
            for i in xrange(len(cells)):
                for j in xrange(len(cells[i])):
                    if ( np.abs(cells[i][j][0] - tr[0]) < 0.001 ) and ( np.abs(cells[i][j][1] - tr[1]) < 0.001 ):
                         cells[i][j] = p
                    
    return cells, originalPoints
                     
           