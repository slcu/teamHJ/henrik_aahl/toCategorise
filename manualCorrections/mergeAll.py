from os import listdir
from os.path import isfile, join
from scipy.spatial.distance import cdist
from scipy.spatial import KDTree
import numpy as np
import cPickle
from mergeData import merge



if __name__ == "__main__":
    myPath = "/home/lisa/Desktop/segmentations_2014_10_07_PGMY_timecourse/0hrs/ALT_plant4_0_4hrs/manually_corrected/"    
    onlyfiles = [ f for f in listdir(myPath) if isfile(join(myPath,f)) ]
    print onlyfiles
    for fName in onlyfiles:
        print fName
        fobj = file(myPath + fName)
        data = cPickle.load(fobj)
        fobj.close()
        cells = data[0]
        points = np.array(data[2])     
        newCells, newPoints = merge(points, cells, distanceThreshold = 2)        
        fobj = file("/home/lisa/Desktop/segmentations_2014_10_07_PGMY_timecourse/0hrs/ALT_plant4_0_4hrs/newPoints/%s.xyz"%fName[:-4], "wb")
        for p in newPoints:
            line = str(p[0]) + " " + str(p[1]) + " " + str(data[-1]) + "\n"
            fobj.write(line)
             
        fobj.close()
        fobj = file("/home/lisa/Desktop/segmentations_2014_10_07_PGMY_timecourse/0hrs/ALT_plant4_0_4hrs/merged/" + fName[:-4] + "merged.pkl", "wb")
        cPickle.dump([newPoints, newCells], fobj)
        fobj.close()  
        
    
    
    
    
    
