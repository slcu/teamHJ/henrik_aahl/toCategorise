from vplants.mars_alt.mars.segmentation import filtering
from openalea.image.spatial_image import SpatialImage
from scipy.ndimage.filters import gaussian_filter
from openalea.image.all import imread, imsave
import matplotlib.path as mplPath
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from scipy import ndimage
import numpy as np
import cPickle
from matplotlib.widgets import Button
 

numberSlices = 147;
folderOne = "/home/lisa/Desktop/segmentations_2014_10_07_PGMY_timecourse/0hrs/ALT_plant4_0_4hrs/"
folderTwo = "/home/lisa/Desktop/segmentations_2014_10_07_PGMY_timecourse/0hrs/ALT_plant4_0_4hrs/"
myFileSegmented = folderOne + "plant4_slow_0hrs-acylYFP_ed_hmin_2_new_clean_3_borderCellsRemoved.tif"
myFileIntensity = folderTwo + "plant4_slow_0hrs-acylYFP_ed.tif"

def segmentedAxDraw():
    global segmentedImageSlice       
    segmentedImageSlice = ndimage.rotate(segmentedImageSlice, 90)
    segmentedImageAx.clear()
    segmentedImageAx.imshow(segmentedImageSlice % segmentedImageColorNb, cmap = cm.jet, origin = "lower")
    segmentedImageAx.autoscale(False)

class segmentedImageViewer(object):
    def __init__(self):
        self.canvas = segmentedImageAx.get_figure().canvas
        self.cid = None
        self.keyboardCid = None
        self.scrollCid = None
        self.previuosKeyPressed = None    
        self.connect_keyboard()
        self.connect_scroll()
        self.connect_sf()
        self.pt_lst = []
        self.pt_plot = segmentedImageAx.plot([], [], marker='.', linestyle='none', zorder=5)[0]
        self.cellsPoints = []
        self.cells = []
        self.cellCounter = 0
        self.correctedImage = np.array(segmentedImageSlice)
        print self.correctedImage.dtype
        self.newLabel = 30000

    def connect_scroll(self):
        if self.scrollCid is None:
            self.scrollCid = self.canvas.mpl_connect('scroll_event', self.scroll_event)
    
    def disconnect_scroll(self):
        if self.scrollCid is not None:
            self.canvas.mpl_disconnect(self.scrollCid)
            self.scrollCid = None        

    def connect_sf(self):
        if self.cid is None:
            self.cid = self.canvas.mpl_connect('button_press_event', self.click_event)

    def disconnect_sf(self):
        if self.cid is not None:
            self.canvas.mpl_disconnect(self.cid)
            self.cid = None
    
    def connect_keyboard(self):
        if self.keyboardCid is None:
            self.keyboardCid = self.canvas.mpl_connect('key_press_event', self.keyboard_event)

    def disconnect_keyboard(self):
        if self.keyboardCid is not None:
            self.canvas.mpl_disconnect(self.keyboardCid)
            self.keyboardCid = None          

    def scroll_event(self, event):
        global segmentedImageSlice, segmentedImageSliceNb
        if  event.button == "up":
            segmentedImageSliceNb += 1
        elif event.button == "down":
            segmentedImageSliceNb -= 1
        if segmentedImageSliceNb > -1 and segmentedImageSliceNb < segmentedImageShape[2]:
            segmentedImageSlice = segmentedImage[..., segmentedImageSliceNb]
        segmentedAxDraw()
        segmentedImageAx.set_title("slice number %d"%segmentedImageSliceNb)
        self.redraw()
        
        
    def keyboard_event(self, event):
        if event.key == 'h':
            self.redrawNew()
        if event.key == "v":
            np.save(folderOne + "corrected_image/corrected_%d.tif"%segmentedImageSliceNb, self.correctedImage)
            print "saved"
            
        if event.key == "i":
            print event.xdata, event.ydata
            eventCoordinates = [int(event.xdata), int(event.ydata)]
            cellInd = self.findCell(eventCoordinates)
            fillValue = segmentedImageSlice[eventCoordinates[1], eventCoordinates[0]] # attention !!!!!!!!!!!! 1, 0
            print "fill value = ", fillValue
            if fillValue == background:
                fillValue = self.newLabel
                self.newLabel += 1
            if cellInd != None:
                print "filling with %d.."%fillValue
                xcoordinates = [self.cells[cellInd][i][0] for i in xrange(len(self.cells[cellInd])) ]
                ycoordinates = [self.cells[cellInd][i][1] for i in xrange(len(self.cells[cellInd])) ]
                bbPath = mplPath.Path(self.cells[cellInd])
                X = []
                Y = []
                for i in xrange(int(min(xcoordinates))- 1, int(max(xcoordinates)) + 1):
                    for j in xrange(int(min(ycoordinates))- 1, int(max(ycoordinates)) + 1):
                        if bbPath.contains_point((i, j)):
                            X.append(i)
                            Y.append(j)
                            self.correctedImage[j, i] = fillValue # attention j, i
                segmentedImageAx.plot(X, Y, ".")
                self.canvas.draw()
                
                inPathCoordinates = set(zip(*[X, Y]))
                labelCoordinates = np.where(self.correctedImage == fillValue)
                labelCoordinates = set(zip(*[labelCoordinates[1], labelCoordinates[0]]))
                labelCoordinates = labelCoordinates - inPathCoordinates  
                for (i, j) in labelCoordinates:
                    print i, j
                    segmentedImageAx.plot([i], [j], ".", color = "y")
                    for SRadius in xrange(1, 10):
                        neigh = set(np.unique(self.correctedImage[j - SRadius : j + SRadius, i - SRadius: i + SRadius]))
                        print "unique ", neigh
                        neigh.remove(fillValue)
#                         neigh.discard(background)
                        print "unique ", neigh
                        if len(neigh) > 0:
                            newVal = neigh.pop()
                            print "newVal ", newVal
                            self.correctedImage[j, i] = newVal
                            break
                            
                
                
                
#                 for i in xrange(segmentedImage.shape[0]):i
#                     for j in xrange(segmentedImage.shape[1]):
#                         if (i, j) not in inPathCoordinates and self.correctedImage[j, i] == fillValue:
#                             print i, j
#                             segmentedImageAx.plot([i], [j], ".", color = "y")
# #                             self.correctedImage[j, i] = 
                self.canvas.draw()           
                             
                                        
    def redrawNew(self):
        fobj = file("/home/lisa/Desktop/segmentations_2014_10_07_PGMY_timecourse/0hrs/ALT_plant4_0_4hrs/merged/cellsCorners_slice_%dmerged.pkl"%segmentedImageSliceNb)
        newPs, newCells = cPickle.load(fobj)
        fobj.close()
        
        self.cells = newCells
        
        x, y = zip(*newPs)
        self.pt_plot.set_xdata(x)
        self.pt_plot.set_ydata(y)
        for nc in newCells:
            X = [item[0] for item in nc]
            X.append(X[0]) 
            Y = [item[1] for item in nc]
            Y.append(Y[0])
            self.lines = segmentedImageAx.plot(X, Y)
        
        self.canvas.draw()     
            
    def findCell(self, coordinates):
        for i in xrange(len(self.cells)):
            bbPath = mplPath.Path(np.array(self.cells[i]))
            if bbPath.contains_point((coordinates[0], coordinates[1])):
                print i, self.cells[i]
                return i
        print "cell not found!"
        return None
    
    def removeLastCell(self):
        l = self.lines.pop(0)
        l.remove()
        self.redraw()
        print "number of cells = ", len(self.cells)

    def click_event(self, event):
        
        ''' Extracts locations from the user'''

        if event.key == 'shift':
            self.pt_lst = []
            self.redraw()
            return

        if event.xdata is None or event.ydata is None:
            return
        if event.button == 1:
#             self.pt_lst.append((event.xdata, event.ydata))
            print self.pt_lst
        elif event.button == 3:
            self.remove_pt((event.xdata, event.ydata))

        self.redraw()

    def remove_pt(self, loc):
        if len(self.pt_lst) > 0:
            self.pt_lst.pop(np.argmin(map(lambda x: np.sqrt((x[0] - loc[0]) ** 2 + (x[1] - loc[1]) ** 2), self.pt_lst)))

    def redraw(self):
        if len(self.pt_lst) > 0:
            x, y = zip(*self.pt_lst)
        else:
            x, y = [], []
        self.pt_plot.set_xdata(x)
        self.pt_plot.set_ydata(y)

        self.canvas.draw()


        
    def redrawFromIntensity(self, newPs, newCells):
        self.pt_lst = newPs
        self.cells = newCells        
        if len(self.pt_lst) > 0:
            x, y = zip(*self.pt_lst)
        else:
            x, y = [], []
        for nc in newCells:
            X = [item[0] for item in nc]
            X.append(X[0]) 
            Y = [item[1] for item in nc]
            Y.append(Y[0])
            self.lines = segmentedImageAx.plot(X, Y)
        
        self.redraw()


def intensityImageAxDraw(rotate = True):
    global intensityImageSlice
    if filter:
        intensityImageSlice = gaussian_filter(intensityImageSlice, 0.5)
    if rotate:   
        intensityImageSlice = ndimage.rotate(intensityImageSlice, 90)    
    intensityImageAx.cla()
    intensityImageAx.imshow(intensityImageSlice, cmap = cm.gray, origin = "lower")
    intensityImageAx.autoscale(False)
    

class intensityImageViewer(object):
    def __init__(self):
        self.canvas = intensityImageAx.get_figure().canvas        
        self.cid = None
        self.keyboardCid = None
        self.scrollCid = None
        self.pt_lst = []
        self.pt_plot = intensityImageAx.plot([], [], marker='.', linestyle='none', zorder=5)[0]
        self.connect_sf()
        self.connect_keyboard()
        self.connect_scroll()
        self.cellsPoints = []
        self.cells = []
        self.cellCounter = 0
        self.scrollCounter = 0
        self.previuosKeyPressed = None        
#         intensityImageAxImage.imshow(self.intImSlice, cmap = cm.gray, origin = "lower")
        

    def connect_scroll(self):
        if self.scrollCid is None:
            self.scrollCid = self.canvas.mpl_connect('scroll_event', self.scroll_event)
    
    def disconnect_scroll(self):
        if self.scrollCid is not None:
            self.canvas.mpl_disconnect(self.scrollCid)
            self.scrollCid = None        

    def connect_sf(self):
        if self.cid is None:
            self.cid = self.canvas.mpl_connect('button_press_event', self.click_event)

    def disconnect_sf(self):
        if self.cid is not None:
            self.canvas.mpl_disconnect(self.cid)
            self.cid = None
    
    def connect_keyboard(self):
        if self.keyboardCid is None:
            self.keyboardCid = self.canvas.mpl_connect('key_press_event', self.keyboard_event)

    def disconnect_keyboard(self):
        if self.keyboardCid is not None:
            self.canvas.mpl_disconnect(self.keyboardCid)
            self.keyboardCid = None          

    def scroll_event(self, event):
        global intensityImageSliceNb, intensityImageSlice
        if  event.button == "up":
            intensityImageSliceNb += 1
        elif event.button == "down":
            intensityImageSliceNb -= 1
        
        if intensityImageSliceNb > -1 and intensityImageSliceNb < intensityImageShape[2]:
            intensityImageSlice = intensityImage[..., intensityImageSliceNb]
        intensityImageAxDraw()
        intensityImageAx.set_title("slice number %d"%intensityImageSliceNb)
        self.redraw()
        
    def removeLastCell(self):
        print "number of cells = ", len(self.cells), "cell " , self.cells[-1], "will be removed."
        self.pt_lst = self.pt_lst[:-len(self.cells[-1])]
        self.cells.pop()
        self.cellsPoints.pop()
        l = self.lines.pop(0)
        l.remove()
        self.redraw()
        print "number of cells = ", len(self.cells)
            

    def keyboard_event(self, event):
        if self.previuosKeyPressed == "control" and event.key == "z":
            print "cancel"
            self.removeLastCell()
            segViewer.removeLastCell()
            segViewer.redrawFromIntensity(self.pt_lst, self.cells)
        self.previuosKeyPressed = event.key

        if event.key == " ":
            self.cellsPoints.append(list(self.pt_lst))
            print len(self.cellsPoints), len(self.cellsPoints[-1]), len(self.pt_lst)
            if len(self.cellsPoints) > 1:
#                 print "now", len(self.cellsPoints[-1]), len(self.cellsPoints[-2])
                lastCellPoints = self.cellsPoints[-1][len(self.cellsPoints[-2]):]
            else:
                lastCellPoints = self.cellsPoints[-1]
            self.cells.append(list(lastCellPoints))
            self.cellCounter += 1
            X = [item[0] for item in lastCellPoints]
            X.append(X[0]) 
            Y = [item[1] for item in lastCellPoints]
            Y.append(Y[0])
            self.lines = intensityImageAx.plot(X, Y)
            self.redraw()      
            segViewer.redrawFromIntensity(self.pt_lst, self.cells)
            return
        
        if event.key == 'h':
            fobj = file("/home/lisa/Desktop/segmentations_2014_10_07_PGMY_timecourse/0hrs/ALT_plant4_0_4hrs/merged/cellsCorners_slice_%dmerged.pkl"%segmentedImageSliceNb)
            newPs, newCells = cPickle.load(fobj)
            fobj.close()
            print type(newPs)
            self.pt_lst = list(newPs)
            self.cells = newCells
            self.redrawPointsCells(newPs, newCells)
            segViewer.redrawFromIntensity(self.pt_lst, self.cells)
        if event.key == "g":
            "pickle"
            if len(self.cells) >0 :
                fobj = file(folder + "manually_corrected/cellsCorners_slice_%d.pkl"%segmentedImageSliceNb, "wb")
                cPickle.dump([self.cells, self.cellsPoints, self.pt_lst, segmentedImageSliceNb], fobj)
                fobj.close()
                print "pickled"
            else:
                print "Nothing to pickle!"

    def click_event(self, event):
        if event.key == 'shift':
            self.pt_lst = []
            self.redraw()
            return
        if event.xdata is None or event.ydata is None:
            return
        if event.button == 1:
            self.pt_lst.append((event.xdata, event.ydata))
            print self.pt_lst
        elif event.button == 3:
            self.remove_pt((event.xdata, event.ydata))
        self.redraw()

    def remove_pt(self, loc):
        if len(self.pt_lst) > 0:
            self.pt_lst.pop(np.argmin(map(lambda x: np.sqrt((x[0] - loc[0]) ** 2 + (x[1] - loc[1]) ** 2), self.pt_lst)))

    def redraw(self):
        if len(self.pt_lst) > 0:
            x, y = zip(*self.pt_lst)
        else:
            x, y = [], []
        self.pt_plot.set_xdata(x)
        self.pt_plot.set_ydata(y)  
        self.canvas.draw()

    def redrawPointsCells(self, pList, cellList):
#         intensityImageAxDraw(rotate = False)
        x, y = zip(*pList)
        self.pt_plot.set_xdata(x)
        self.pt_plot.set_ydata(y)
        for nc in cellList:
            X = [item[0] for item in nc]
            X.append(X[0]) 
            Y = [item[1] for item in nc]
            Y.append(Y[0])
            self.lines = intensityImageAx.plot(X, Y)
        self.canvas.draw()

if __name__ == "__main__":
    segmentedImageSliceNb = numberSlices
    background = 1
    segmentedImageFName = myFileSegmented
    intensityImageFname = myFileIntensity
    segmentedImage = imread(segmentedImageFName)

    
    segmentedImageShape = segmentedImage.shape
    segmentedImageSlice = segmentedImage[..., segmentedImageSliceNb]
    segmentedImageSlice = ndimage.rotate(segmentedImageSlice, 90)
    segmentedImageRowsNb, segmentedImageColsNb = segmentedImageSlice.shape
    segmentedImageColorNb = 64

    def SegmentedFormat_coord(x, y):
        col = int(x + 0.5)
        row = int(y + 0.5)
        if col >= 0 and col < segmentedImageColsNb and row >= 0 and row < segmentedImageRowsNb:
            z = segmentedImageSlice[row, col]
            return 'x=%1.4f, y=%1.4f, value=%1.4f'%(x, y, z)
        else:
            return 'x=%1.4f, y=%1.4f'%(x, y)
    
    segmentedImageFig = plt.figure("Segmented Image, v for save ")
    
    segmentedImageAx = segmentedImageFig.gca()
    segmentedImageAx.imshow(segmentedImageSlice %segmentedImageColorNb, cmap = cm.jet, origin = "lower")
    
    
    segmentedImageAx.autoscale(False)
    segViewer = segmentedImageViewer()
    
    segmentedImageAx.format_coord = SegmentedFormat_coord
    segmentedImageAx.set_title("slice number %d"%segmentedImageSliceNb)
    
    
    
    intensityImageSliceNb = segmentedImageSliceNb
    filter = True 
    
    intensityImage = imread(intensityImageFname)
    intensityImageShape = intensityImage.shape
    intensityImageSlice = intensityImage[..., intensityImageSliceNb]
    if filter:
        intensityImageSlice = gaussian_filter(intensityImageSlice, 0.5)   
    intensityImageSlice = ndimage.rotate(intensityImageSlice, 90)
    numrows, numcols = intensityImageSlice.shape

    def format_coord(x, y):
        col = int(x+0.5)
        row = int(y+0.5)
        if col>=0 and col<numcols and row>=0 and row<numrows:
            z = intensityImageSlice[row,col]
            return 'x=%1.4f, y=%1.4f, value=%1.4f'%(x, y, z)
        else:
            return 'x=%1.4f, y=%1.4f'%(x, y)
    intensityImageFig = plt.figure("Intensity image figure, \"g\" for pickle ")
    
    intensityImageAx = intensityImageFig.gca()
    intensityImageAx.imshow(intensityImageSlice, cmap = cm.gray, origin = "lower")
    
    
    intensityImageAx.autoscale(False)
    intImageViewer = intensityImageViewer()
    
    intensityImageAx.format_coord = format_coord
    intensityImageAx.set_title("slice number %d"%intensityImageSliceNb)
    plt.autoscale(False)
    

     
    
    plt.show()

