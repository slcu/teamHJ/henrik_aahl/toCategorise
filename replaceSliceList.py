from openalea.image.spatial_image import SpatialImage
from openalea.image.all import imread, imsave
import numpy as np
import random

datapath = "/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2015_1_13_PGMYCR/plant2/stackreg/" 
#after running this must convert images to 8-bit
data = [
["0hrs", []],
["6hrs", []],
["12hrs", []],
["18hrs", []],
["24hrs", [190, 192]],
["30hrs", [12, 13, 22]],
["36hrs", []],
["42hrs", [23, 25, 55, ]],
["48hrs", []], 
["54hrs", [9, 11,12, 14, 18, 42, 45, 67]],
["60hrs", []],
["66hrs", [26, 31, 62, 80, 113, 124]],
["72hrs", [22, 23, 26, 39, 53, 76,140]],
["78hrs", [9]],
["78hrs_central_zone", []]
]


for timepoint in data:
    originalFileName = datapath + "plant2_" +timepoint[0] + "-clv3.tif"
    sliceNumbers = timepoint[1]

    image = imread(originalFileName)
    newImage = np.ones(shape = (image.shape[0], image.shape[1], image.shape[2]), dtype = np.uint16)
    sliceNumbers = [sl-1 for sl in sliceNumbers] #data array shifted by 1
    sliceNumbers = sorted(sliceNumbers)
    if 0 in sliceNumbers or image.shape[2]-1 in sliceNumbers: 
        print "first and last slice cannot be replaced"
        quit()

    i = 0
    while i < len(sliceNumbers):
        sl = sliceNumbers[i]
        print sl
        nConsSlices = 1
        sld = sl         
        while (sld + 1 in sliceNumbers):
            nConsSlices = nConsSlices + 1
            sld = sld + 1
        
        print nConsSlices
 
        if nConsSlices%2 == 0: 
            for j in range(0, nConsSlices/2): newImage[..., sl + j] = image[..., sl - 1]; print "replacing slice", sl + j,  " with ", sl-1  
            for j in range(nConsSlices/2, nConsSlices): newImage[..., sl + j] = image[..., sl + nConsSlices]; print "replacing slice", sl + j, " with ",  sl + nConsSlices   
        else:
            for j in range(0, (nConsSlices-1)/2): newImage[..., sl + j] = image[..., sl - 1]; print "replacing slice", sl + j, " with ", sl-1 
            newImage[..., sl + (nConsSlices-1)/2] = image[..., sl + nConsSlices]; print "replacing slice", sl + (nConsSlices-1)/2, " with ", sl + nConsSlices    
            for j in range((nConsSlices+1)/2, nConsSlices): newImage[..., sl + j] = image[..., sl + nConsSlices]; print "replacing slice", sl + j, " with ", sl + nConsSlices         
        i = i + nConsSlices  


    for sn in xrange(image.shape[2]):
        if sn not in sliceNumbers:
            newImage[..., sn] = image[..., sn]


    imsave(originalFileName[:-4] + "slReplaced.tif", SpatialImage(newImage))
