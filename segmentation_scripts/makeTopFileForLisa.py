import cPickle
from tissueviewer.image import extractL1L2
from tissueviewer.tvtiff import tiffread
import numpy as np
import os

dirPath = "/home/yassin/home3/Data/Lisa/plant13New/final/cleaned_3/"

# propFile = "/home/yassin/home3/Data/Yassin/plant8_trimmedfiles/slicesCorrected/clean_3/0hrs_plant8-acylYFP_trim_rep_hmin_2_asf_2.00_s_1.50_new_clean_3_50.pkl"
fileNames = [dirPath + fn for fn in os.listdir(dirPath) if fn[-4:] == ".pkl"]
print len(fileNames)



#     data = {"labels": labels, "volumes" : volumes, "barycenter" : barycenters, "L1": L1, "wall_surface": wall_surface, "background_neighbors": neighborhood[1], "neigbourhood": neighborhood}

# fileName = "/home/yassin/home3/Data/Lisa/plant13New/final/cleaned_3/0hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_hf_clean_3_selectedCellsRemoved_labelsOK_50_forLisa.pkl"
# 
# import cPickle        
# fobj = file(fileName)
# data = cPickle.load(fobj)
# fobj.close()
# 
# print data.keys()
# exit()

# fileNames = ["/home/yassin/home3/Data/Lisa/plant13New/final/cleaned_3/48hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_hf_clean_3_50.pkl"]



for propFile in fileNames:
    print propFile
    
    fobj = file(propFile)
    graph_55hours, cellsP_55hours, contactArea_55hours, contactAreaCleaned_55hours = cPickle.load(fobj)
    fobj.close()
    
    tiffName =  propFile[:-7] + ".tif"
    print tiffName
    
    image, tags = tiffread(tiffName)
    image = np.swapaxes(image, 2, 0)
    
    L1, L2 = extractL1L2(image, 1)
#     exit()
    
    barycenters = dict((cid, P["center"]) for cid, P in cellsP_55hours.iteritems() if cid != 1)
    volumes = dict((cid, P["volume"]) for cid, P in cellsP_55hours.iteritems() if cid != 1)
    
    labels = barycenters.keys()
    
    
    contactAreaFinal_55hrs = dict()
    
    for edge, cA in contactAreaCleaned_55hours.iteritems():
        if cA > 0:
            contactAreaFinal_55hrs[edge] = cA
            
    neighborhood = dict()        
    for item in contactAreaFinal_55hrs:
        neighborhood.setdefault(item[0], set()).add(item[1])
        neighborhood.setdefault(item[1], set()).add(item[0])
    
    wall_surface = contactAreaFinal_55hrs
    data = {"labels": labels, "volumes" : volumes, "barycenter" : barycenters, "L1": L1, "wall_surface": wall_surface, "background_neighbors": neighborhood[1], "neigbourhood": neighborhood}
        
    fobj = file(propFile[:-4]+ "_forLisa.pkl", "w")
    cPickle.dump(data, fobj)
    fobj.close()
