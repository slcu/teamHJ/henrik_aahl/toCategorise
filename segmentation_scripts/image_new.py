############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@slcu.cam.ac.uk>
# Copyright (c) 2012 - 2016, Yassin Refahi
# All rights reserved.
# Redistribution and use in source and binary forms, with or without
# modification, are not permitted.
#
############################################################################

import os
import pickle
import cPickle
import zipfile
import numpy as np
import scipy.ndimage as nd
from scipy.ndimage.morphology import binary_dilation
from tissueviewer.tvtiff import tiffread, tiffsave, getTiffFileResolution
from scipy.spatial.distance import cdist


def duplicateZ(imageFileName):
    im, tags = tiffread(imageFileName)
    print tags
    imDes = tags["ImageDescription"].split("\n")
    newDes = ""
    for k in imDes:
        if k.startswith("images="):
            print int(k[7:])
            newDes += "images=%d\n"%(2 * int(k[7:]))
        elif k.startswith("slices="):
            print int(k[7:])
            newDes += "slices=%d\n"%(2 * int(k[7:]))
        elif k.startswith("spacing="):
            print float(k[8:])    
            newDes += "spacing=%f\n"%(0.5 * float(k[8:]))
        elif len(k) > 0:
            newDes += k
            newDes += "\n"    
    print newDes
    tags["ImageDescription"] = newDes
    print tags
    
    im = np.swapaxes(im, 2, 0) ### Attention    
    newIm = np.ones(shape = (im.shape[0], im.shape[1], im.shape[2] * 2), 
                    dtype = im.dtype)
    for i in xrange(newIm.shape[2]):
        if i % 2 == 0:
            newIm[..., i] = im[..., i/ 2]
    for i in xrange(newIm.shape[2] - 1):
        if i % 2 == 1:
            newIm[..., i] = newIm[..., i - 1]
    
    newIm = np.swapaxes(newIm, 2, 0)        
    tiffsave(newIm, imageFileName[:-4] + "_rm.tif", **tags)
    return newIm 



duplicateZ(imageFileName = "/home/lisa/home3/data/20170213TimelapseMTMutants/processed/fas2-1Ler/fas2-1 line3 11_trim_stackreg.tif")
