from tissueviewer.segmentation import cleanWithoutRelabeling

fList = [
"/home/lisa/home3/asymmetric_divisions/processed_segmented_stacks_MT_mutants_etc/fas 2-1 Ler line 3 shape phenotype/fas2-1 line3 11_trim_stackreg_rm_hmin_2_asf_2.00_s_1.50_mod_clean_3_mod.tif"
]
param = 3

for fName in fList:
    cleanWithoutRelabeling(fName, param)

