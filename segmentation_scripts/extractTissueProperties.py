from tissueviewer.mesh import *
from tissueviewer.tvtiff import tiffread
from time import time
import cPickle
from tissueviewer.image import extractL1L2
from tissueviewer.tvtiff import tiffread
import numpy as np
import os


names = ["/home/lisa/home3/asymmetric_divisions/processed_segmented_stacks_MT_mutants_etc/fas 2-1 Ler line 3 shape phenotype/fas2-1 line3 5_trim_stackreg_rm_hmin_2_asf_2.00_s_1.50_mod_clean_3.tif",
"/home/lisa/home3/asymmetric_divisions/processed_segmented_stacks_MT_mutants_etc/fas 2-1 Ler line 3 shape phenotype/fas2-1 line3 9_trim_stackreg_rm_hmin_2_asf_2.00_s_1.50_mod_clean_3.tif",
"/home/lisa/home3/asymmetric_divisions/processed_segmented_stacks_MT_mutants_etc/fas 2-1 Ler line 3 shape phenotype/fas2-1 line3 11_trim_stackreg_rm_hmin_2_asf_2.00_s_1.50_mod_clean_3_mod_clean_3.tif"]

stretch_factor = (1.08, 1.03, 1.03)

for i in range(len(names)):
    segFName = names[i]
    contactAreaThreshold = 0.7
    smoothSteps = 50
    print segFName
    image, tags = tiffread(segFName)
    image = np.swapaxes(image, 2, 0)
    t1 = time()
    resolution = getTiffFileResolution(segFName)
# take into account the streching factor
    resolution = resolution[0], resolution[1], resolution[2]/stretch_factor[i]
    print resolution
    tA = TissueAnalysis(image, resolution, removeBoundaryCells=False)
    tA.computeVolumes()
    tA.meshExtractCellProperties(reduction = 0, smooth_steps = smoothSteps)
    tA.makeGraph()
    tA.graph.removeDoubleEdges()
    tA.meshExtractContactArea(reduction = 0, smooth_steps = smoothSteps)
    tA.checkCleanContactArea(contactAreaThreshold)
        
    
    fobj = file(segFName[:-4] + "_%d.pkl"%smoothSteps, "w")
    cPickle.dump((tA.graph, tA.cellsProperties, tA.contactArea, tA.contactAreaCleaned), fobj)
    fobj.close()
    
    graph_55hours, cellsP_55hours, contactArea_55hours, contactAreaCleaned_55hours = tA.graph, tA.cellsProperties, tA.contactArea, tA.contactAreaCleaned
    
    
    L1, L2 = extractL1L2(image, 1)
    
    barycenters = dict((cid, P["center"]) for cid, P in cellsP_55hours.iteritems() if cid != 1)
    volumes = dict((cid, P["volume"]) for cid, P in cellsP_55hours.iteritems() if cid != 1)
    
    labels = barycenters.keys()
    
    
    contactAreaFinal_55hrs = dict()
    
    for edge, cA in contactAreaCleaned_55hours.iteritems():
        if cA > 0:
            contactAreaFinal_55hrs[edge] = cA
            
    neighborhood = dict()        
    for item in contactAreaFinal_55hrs:
        neighborhood.setdefault(item[0], set()).add(item[1])
        neighborhood.setdefault(item[1], set()).add(item[0])
    
    wall_surface = contactAreaFinal_55hrs
    data = {"labels": labels, "volumes" : volumes, "barycenter" : barycenters, "L1": L1, "wall_surface": wall_surface, "background_neighbors": neighborhood[1], "neigbourhood": neighborhood}
        
    fobj = file(segFName[:-4]+ "_forLisa.pkl", "w")
    cPickle.dump(data, fobj)
    fobj.close()

    print "Elapsed time : ", time() - t1
    

