from openalea.image.all import imread, SpatialImageAnalysis
from openalea.image.spatial_image import SpatialImage
from concatenate_labels import concatLabels
import scipy.ndimage as nd
import numpy as np
from time import time
from PINConfig import fileName, PINFileName, resolution, organismFormatPINDataFileName, maxVolume


def bbDilation(slices):
    return [ slice(max(0, s.start - 1), s.stop + 1) for s in slices ]

def wallVoxelsBetweenCells(image, label_1, label_2, bbox):
    boundingbox_1 = bbox[label_1]
    dilated_bbox_1 = bbDilation( boundingbox_1)
    dilated_bbox_1_img = image[dilated_bbox_1]
    mask_img_1 = (dilated_bbox_1_img == label_1)
    mask_img_2 = (dilated_bbox_1_img == label_2)
    struct = nd.generate_binary_structure(3, 2) # here we define how the walls will be selected
    dil_1 = nd.binary_dilation(mask_img_1, structure=struct)
    dil_2 = nd.binary_dilation(mask_img_2, structure=struct)
    x, y, z = np.where( ( (dil_1 & mask_img_2) | (dil_2 & mask_img_1) ))
    x_1, y_1, z_1 = np.where( (dil_2 & mask_img_1) )
    x_2, y_2, z_2 = np.where( (dil_1 & mask_img_2) )
    xToAdd = dilated_bbox_1[0].start
    yToAdd = dilated_bbox_1[1].start
    zToAdd = dilated_bbox_1[2].start
    return ( (np.array(x) + xToAdd, np.array(y) + yToAdd, np.array(z) + zToAdd) ), ( (np.array(x_1) + xToAdd, np.array(y_1) + yToAdd, np.array(z_1) + zToAdd) ), ( (np.array(x_2) + xToAdd, np.array(y_2) + yToAdd, np.array(z_2) + zToAdd) )

t1 = time()

# im = SpatialImage(concatLabels(imread(fileName)[ :256, :256, :100])[0])
# yassinOriginalIm = imread(fileName)
im, mapping = concatLabels(imread(fileName))
im = SpatialImage(im)
# print mapping
if mapping == None:
    mapping = [(i, i) for i in xrange(np.max(im) + 1)]
inverseMapping = dict()
for m in mapping:
    inverseMapping[m[1]] = m[0]
# print inverseMapping    
im.resolution = resolution
PINIm = imread(PINFileName)
print im.shape, PINIm.shape
imAnalysis = SpatialImageAnalysis(im)




print "Elapsed time to read the image = ", time() - t1
background = 1
labels = np.unique(im)
#labels = imAnalysis.labels()



print "Elapsed time to read the image, find labels = ", time() - t1

volumes = dict( zip( labels, imAnalysis.volume(labels, real = True) ) )

print "Elapsed time to read the image, find labels, calculate volumes  = ", time() - t1

# newLabels = []
# 
# # for lab in labels:
# #     if volumes[lab] < maxVolume:
# #         newLabels.append(lab)
#     
# newLabels.append(background)
# labels = newLabels


bboxesHere = nd.find_objects(im, max_label = np.max(im) + 1)

bboxesDict = dict()
for i in xrange(len(labels)):
    bboxesDict.setdefault(labels[i],  bboxesHere[i])
    

print "Elapsed time to read the image, find labels, calculate volumes, bounding boxes = ", time() - t1 


# labelsCleaned = set(labels).discard(background)
labelsCleaned = set(labels)

neighborhood = imAnalysis.neighbors(list(labelsCleaned)) # neighborhood is a dictionary of cell labels and the list of corresponding neighbors i.e. {cellLable: [NeighborCellLable_1, ...], ..}
# newNeighborhood = dict()
# for cid, N in neighborhood.iteritems():
#     newN = [n for n in N if n in labels]
#     newNeighborhood[cid] = newN
#     
# neighborhood = newNeighborhood 
    
    
print "Elapsed time to read the image, find labels, calculate volumes, bounding boxes, extract neighborhood = ", time() - t1



edges = []
for source, targets in neighborhood.iteritems():
    if source in labels:
        for target in targets:
            if source < target and target in labels:
                edges.append((source, target))
edgesDict = dict((source, []) for source, target in edges)
for source, target in edges:
    edgesDict[source].append(target)


wall_surface = imAnalysis.wall_surfaces(edgesDict, real = True)
print "Elapsed time to read the image, find labels, calculate volumes, bounding boxes, extract neighborhood, extract wall surfaces = ", time() - t1

barycenter = dict( zip( labels, imAnalysis.center_of_mass(labels, real = True) ) )
print "Elapsed time to read the image, find labels, calculate volumes, bounding boxes, extract neighborhood, extract barycenters = ", time() - t1





fobj = file(organismFormatPINDataFileName, "w")
varNb = 6
fobj.write(str(len(labels)) + " %d \n"%varNb)


for label in labels:
    line = ""
    print label
    if label != 1:
        line += str(inverseMapping[label])
        line += str(" " + str(volumes[label]))
        line += str(" " + str( np.power(volumes[label], 1.0 /3.0) ) )
        line += str(" " + str(barycenter[label])[1:-1]) 
        line += str(" " + str(len(neighborhood[label])) )
        print "Number of neighbors = ", len(neighborhood[label]), " volume = ", volumes[label]
        for Neig in neighborhood[label]:
            line += str(" " + str(inverseMapping[Neig]))
        for Neig in neighborhood[label]:
            if label < Neig:
                p = (label, Neig)
            else:
                p = (Neig, label)
            line += str(" " + str(wall_surface[p])) 
        for label_2 in neighborhood[label]:
            voxelsOnWallsCoordinates = wallVoxelsBetweenCells(im, label, label_2, bboxesDict)
            print "important", label, label_2, np.unique(im[voxelsOnWallsCoordinates[0]]), np.unique(im[voxelsOnWallsCoordinates[1]]), np.unique(im[voxelsOnWallsCoordinates[2]])
#             vals = np.sum(PINIm[voxelsOnWallsCoordinates[0]]), np.sum(PINIm[voxelsOnWallsCoordinates[1]]), np.sum(PINIm[voxelsOnWallsCoordinates[2]])
            vals = np.sum(PINIm[voxelsOnWallsCoordinates[1]])
            print len(voxelsOnWallsCoordinates[1][0]), len(voxelsOnWallsCoordinates[0][0]), len(voxelsOnWallsCoordinates[2][0])
#             raw_input()
#             line += str(" " + str(len(voxelsOnWallsCoordinates[1])))
            line += str(" " + str(len(voxelsOnWallsCoordinates[1][0])))
            line += str(" " + str(vals))
        line += "\n"
    fobj.write(line)
            
fobj.close()
print "Elapsed time = ", time() - t1
