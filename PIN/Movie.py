#This script takes a series of images and combines them, pasting the images into a second series
#The final line is to be run at the command in the folder where all tiffs for the movie are in the correct order
# -*- coding: utf-8 -*-
import Image
import os
import sys

aux_text=Image.open('aux_text.png')
mp_text=Image.open('mp_text.png')
ath_text=Image.open('ath_text.png')

for i in range(1,100):
  i_str = str(i)
  sys.stdout.write(str(i)+" ")
  sys.stdout.flush()
  #if i < 1000 : i_str = '0'+i_str
  if i < 100 : i_str = '0'+i_str
  if i < 10 : i_str = '0'+i_str
  aux = Image.open('temp/aux'+i_str+'.tif')
  MP1crepressor = Image.open('temp/1c_both_MP'+i_str+'.tif')
  ath1crepressor = Image.open('temp/1c_both_ATHB8'+i_str+'.tif')
  m = Image.new('RGB',(360,780))
  m.paste(aux,(0,0))
  m.paste(MP1crepressor,(0,240))
  m.paste(ath1crepressor,(0,480))
  m.paste(aux_text,(110,90))
  m.paste(mp_text,(110,320))
  m.paste(ath_text,(110,560))
  outfile = 'temp2/merged'+i_str+'.tif'
  m.save(outfile)

sys.stdout.write("\n")
#mencoder mf://*tif -mf fps=10 -ovc xvid -xvidencopts bitrate=1200 -o growth.avi
