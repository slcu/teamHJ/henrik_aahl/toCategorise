#reads in lineage tree in format
#t0 m01 m02 m03 ...
#t1 m11 m12 m13 ...
#t2 m21 m22 m23 ...

#then from corresponding time files reads in corresponding variable 
#t0 v01 v02 v03 ...
#t1 v11 v12 v13 ...
#t2 v21 v22 v23 ...

import numpy as np

lineage = open("lineage48hprimordia.txt", "r")

dataFolder = "./results_variables/"
fileName = "organismFormatPINDATA_timePoint_"
tag = "h.txt"

lineageArr = []

for l in lineage.readlines():
#read in time and load in files accordingly
    l = l.split(" ")
    while "" in l: l.remove("")
    l[-1] = l[-1][:-1] #remove /n final character
    l = [int(i) for i in l]
    print l
    lineageArr.append(l)

lineage.close()  

results = []

print lineageArr

for t in range(0,10):
    #read in file 
    time = lineageArr[t][0];
    print time
    dataFile = open(dataFolder + fileName + str(time) + tag, "r")
    data = dataFile.readlines()
    dataFile.close()
    results.append([time])

    #collect all data into array of proper types 
    dataCellNum = len(data) - 1 
    dataArr = []
    for i in range(1, dataCellNum  + 1):
        d = data[i].split(" ")	
        while "" in d: d.remove("")	
        d[-1] = d[-1][:-1] #remove /n final character
        newd = [float(num_string) for num_string in d] #convert all numbers to floats
        newd[0] = int(newd[0]) # cell label
        for j in range(6, 7 + int(d[6])): # approp. floats to integers
           newd[j] = int(newd[j])   
        for j in range(7 + 2*int(d[6]), 7 + 3*int(d[6])): 
           newd[j] = int(newd[j])  
        #select variables to be recorded
        #dataArr.append([newd[0], newd[1]])
        dataArr.append(newd) 

    for ind in lineageArr[t][1:]: 
        totPIN = 0
        totN = 0 
        for j in range(0, dataCellNum): 
            if dataArr[j][0] == ind:
                numNeigh =  dataArr[j][6]
                for k in range(0, numNeigh):
                    totN += dataArr[j][7 + 2*numNeigh + 2 * k]  #num voxels in wall       
                    totPIN += dataArr[j][7 + 2*numNeigh + 2 * k + 1]  #PIN in wall  

                totPIN = float(totPIN)/float(totN)
                results[-1].append(totPIN)            
 
#write output file of variable evolution for each cell 
output = open("resultsPIN48hPrimordia.txt","w")
for i in range(0, len(results)):
    s =  str(results[i][0]) 
    for j in range(1, len(results[i])):
        s += " " +  str(results[i][j])
    s += "\n"
    output.write(s)
output.close()

output = open("modResults48hPrimordia.txt","w")

#calculate total volume change 
for i in range(0, len(results)):
    s =  str(results[i][0]) 
    tot = 0
    cells = []
    for j in range(1, len(results[i])):
        if lineageArr[i][j] not in cells: 
            cells.append(lineageArr[i][j])
            tot += results[i][j]
    s += " " +  str(tot) + "\n"
    output.write(s)

output.close()


