import cPickle
import numpy as np

fobj = file("lineageTree.pkl")
linTree = cPickle.load(fobj)
fobj.close()

#time points must be consecutive data points in lineage tree
timePoint = [48, 44, 40, 36, 32, 28, 24, 20, 16, 12, 8, 4, 0]

#cell labels of primordia
cellsPrimordia48h = [[1699, 1868, 1890, 1899, 1900, 1906, 1907, 1926, 1931, 1944, 1945, 1958, 1959, 1960, 1962, 1975, 1976, 1981, 1996, 2004, 2005, 2006, 2034, 2069, 2071, 2091, 2100, 2105, 2113, 2134, 2166, 2167, 2177, 2198, 2199, 2201, 2202, 2206, 2217, 2218, 2219, 2221, 2222, 2224, 2225, 2237, 2245, 2246, 2247, 2256, 2257, 2260, 2261, 2283, 2297, 2298, 2299, 2306, 2307, 2314, 2318, 2336, 2337, 2340, 2341, 2351, 2352, 2368, 2381, 2384, 2385, 2408, 2411, 2424, 2426, 2427, 2432, 2434, 2435, 2436, 2459, 2460, 2462, 2463, 2464, 2465, 2467, 2487, 2492, 2505, 2506, 2517, 2524, 2528, 2529, 2537, 2538, 2544, 2545, 2557, 2575, 2576, 2586, 2587, 2599, 2601, 2602, 2603, 2606, 2627, 2630, 2633, 2636, 2641, 2645, 2646, 2652, 2654, 2675, 2676, 2677, 2679, 2692, 2693, 2694, 2704, 2705, 2727, 2742, 2766, 2768, 2769, 2790, 2804, 2807, 2809, 2820, 2821, 2822, 2833, 2835, 2836, 2839, 2842, 2843, 2855, 2856, 2866, 2867, 2868, 2874, 2876, 2877, 2882, 2883, 2884, 2886, 2892, 2894, 2898, 2899, 2905, 2906, 2921, 2925, 2926, 2944, 2945, 2946, 2949, 2950, 2968, 2969, 2971, 2972, 2973, 2974, 2975, 2979, 2989, 2992, 2993, 2997, 3010, 3011, 3014, 3015, 3017, 3029, 3031, 3042, 3044, 3045, 3081, 3093, 3097, 3109, 3110, 3111, 3112, 3113, 3114, 3115, 3123, 3124, 3133, 3144, 3145, 3149, 3150, 3158, 3160, 3177, 3179, 3180, 3188, 3195, 3201, 3202, 3212, 3216, 3217, 3222, 3223, 3229, 3230, 3233, 3241, 3250, 3260, 3261, 3263, 3276, 3278, 3279, 3284, 3285, 3286, 3287, 3296, 3306, 3307, 3310, 3312, 3322, 3336, 3337, 3339, 3340, 3353, 3354, 3361, 3362, 3363, 3364, 3365, 3367, 3380, 3404, 3405, 3406, 3431, 3435, 3448, 3466, 3469, 3470, 3471, 3472, 3473, 3484, 3498, 3501, 3511, 3515, 3524, 3531, 3544, 3545, 3546, 3558, 3567, 3576, 3577, 3596, 3609, 3615, 3624, 3635, 3637, 3639, 3645, 3659, 3662, 3663, 3665, 3676, 3677, 3695, 3713, 3724, 3771, 3825]]


print(len(cellsPrimordia48h[0]))

for i in range(0, len(timePoint) - 1):
	cellsPrimordia48h.append( [-1]* len(cellsPrimordia48h[0]))

#traceback all cells in list
for i in range(0, len(timePoint) - 1):
    for m, dList in linTree.iteritems():
        if m / 10000 == (timePoint[i] - 4):
            for d in dList:
                dCell = d % 10000
                if dCell in cellsPrimordia48h[i]:
                    ind = cellsPrimordia48h[i].index(dCell)   
                    cellsPrimordia48h[i+1][ind] =  m % 10000

cellsPrimordia48h = np.array(cellsPrimordia48h)

#remove cells that cannot be traced back all the way
cellsRemove = []
for i in range(0, len(cellsPrimordia48h[-1])):
    if cellsPrimordia48h[-1][i] == -1:
        cellsRemove.append(i)

for i in range(0, len(cellsRemove)): 
    cellsPrimordia48h = np.delete(cellsPrimordia48h, cellsRemove[i]-i, 1)           


#write output file  
output = open("lineage48hprimordia.txt","w")

print(len(cellsPrimordia48h[0]))
l = len(cellsPrimordia48h) - 1
for i in range(0, l + 1):
    s =  str(timePoint[l - i]) 
    for j in range(0, len(cellsPrimordia48h[0])):
        s += " " +  str(cellsPrimordia48h[l-i][j])
    s += "\n"
    output.write(s)

output.close()
