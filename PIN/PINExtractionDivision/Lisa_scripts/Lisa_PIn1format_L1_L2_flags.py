# takes MARS file with label 1 = background. Adds L1 flag to last-1 position in each row, and L2 flag to last position in each row.
#

import math
import cPickle

dataPath = "/home/lisa/Desktop/Lisa_PIN1_analysis_July_1014/PINExtractionDivision_plant13/PINExtractionData/"
destPath = "/home/lisa/Desktop/Lisa_PIN1_analysis_July_1014/PINExtractionDivision_plant13/PINExtractionDataLFlags/"
fileNameSup = "organismFormatPINDATA_timePoint_"
timeInput = open(destPath + "times.txt", "r")

for t in timeInput.readlines():
#read in time and load in files accordingly
   tStr = t.split(" ")
   while "" in tStr: tStr.remove("")
   time = int(tStr[0])
   fileName = fileNameSup + str(time) + "h.txt"
   dataFile = open(dataPath + fileName,"r")
   data = dataFile.readlines()
   dataFile.close()
   dataArr = []

#collect data in array and convert to correct numerical type; row i of dataArr corresponds to cell label i + 2 (label 1 is background).
   for i in range(1,len(data)):
      d = data[i].split(" ")	
      while "" in d: d.remove("")	
      d[-1] = d[-1][:-1] #remove /n final character
      newd = [float(num_string) for num_string in d] #convert all numbers to floats
      newd[0] = int(newd[0])
      for j in range(6, 7 + int(d[6])): # approp. floats to integers
         newd[j] = int(newd[j])   
      for j in range(7 + 2*int(d[6]), 7 + 3*int(d[6])): 
         newd[j] = int(newd[j])   
      dataArr.append(newd)

#add L1 flag at end of each row 
   for i in range(0,len(dataArr)):
      lOne = False		
      for j in range(7, 7 + dataArr[i][6]):
         if dataArr[i][j] == 1:
            lOne = True
      if lOne: dataArr[i].append(1)
      else: 
         dataArr[i].append(0)

#add L2 flag at end of each row. Comment out if require just L1
   newDataArr = []	
   for i in range(0,len(dataArr)):
      newDataArr.append(dataArr[i])
      if (dataArr[i][-1] != 1): # only proceed for non-L1 cells
         lTwo = False
         for j in range(7, 7 + dataArr[i][6]):
            neigh = dataArr[i][j]
            if neigh != 1: #only non-background neighbours
              if ((neigh < 2) | (neigh-2 >= len(dataArr))): print("neigh out of bounds "+str(dataArr[j])) 
              elif dataArr[neigh-2][-1] == 1:
                 lTwo = True
       	 if lTwo: newDataArr[i].append(1)
	 else: newDataArr[i].append(0)
      else: newDataArr[i].append(0) #L1 cells cannot be L2 cells

#write output file  
   output = open(destPath + fileNameSup + str(time) + "h_Lflags.txt","w")
   output.write("%s" % data[0])	
	
   for i in range(1,len(data)):
      data[i] = str(newDataArr[i-1][0])
      for j in range(1, len(newDataArr[i-1])):
         data[i] += " " + str(newDataArr[i-1][j])
      data[i] += "\n"        
      output.write("%s" % data[i])
   output.close()

timeInput.close()
