import math
import cPickle

dataPath = "/home/lisa/Desktop/Lisa_PIN1_analysis_July_1014/PINExtractionDivision_plant13/PINExtractionData/"
destPath = "/home/lisa/Desktop/Lisa_PIN1_analysis_July_1014/PINExtractionDivision_plant13/PINExtractionDataMod/"
fileNameSup = "organismFormatPINDATA_timePoint_"
timeInput = open(destPath + "times.txt", "r")

for t in timeInput.readlines():
#read in time and load in files accordingly
    tStr = t.split(" ")
    while "" in tStr: tStr.remove("")
    time = int(tStr[0])
    print(time)
    fileName = fileNameSup + str(time) + "h.txt"
    dataFile = open(dataPath + fileName,"r")
    data = dataFile.readlines()
    dataFile.close()

#write output file  
    output = open(destPath + fileNameSup + str(time) + "h_mod.txt","w")
	
    output.write("%i\n" % (1))
    output.write("%i %i %i\n" % (len(data)-1, 5, 0))	# organism format: num. cells, num. variables, timepoint

    for i in range(1,len(data)):
	d = data[i].split(" ")	
	while "" in d: d.remove("")
        d[-1] = d[-1][:-1] #remove /n final character
        output.write("%f %f %f %f %f\n" % (float(d[3]),float(d[4]),float(d[5]),float(d[2]),float(d[1])))
    output.close()

timeInput.close()
