from openalea.image.algo.graph_from_image import SpatialImageAnalysis
from openalea.image.all import imread, imsave, SpatialImage
from time import time
import cPickle
import numpy as np
import scipy

hour = 12
hmin = 3
width = 2

L1PericlinalWallErosionIteration = 1
surfaceVoxelsDilationIteration = 2

commonPath = "/media/Seagate Expansion Drive/storage/data/Lisa/LisaImages/7Oct2013/8hrs/"
segmentedImageFileName = commonPath + "backGroundCorrected/" +        "8hrs_Plant13-acylYFP_hmin_2_clean_3_bk_ok.tif" 
cellWallsFileName = commonPath + "walls/" +                           "8hrs_Plant13-acylYFP_hmin_2_clean_3_bk_ok_walls_width_2.npy"
pinConcentrationFileName = commonPath +                               "tifs/8hrs_plant13-PIN.tif"
label2ExpressionValuesFile = commonPath + "label2ExpressionValues/" + "label2ExpressionValue_8hrs_Plant13-acylYFP_hmin_2_clean_3_bk_ok_walls_Width_%d_erosion_%d_surfaceDil_%d.pkl"%(width, L1PericlinalWallErosionIteration, surfaceVoxelsDilationIteration)

cellWalls = np.load(cellWallsFileName)
pinImage = imread(pinConcentrationFileName)

tissueImageData = imread(segmentedImageFileName)
imageShape = tissueImageData.shape
print "imageData shape:", tissueImageData.shape
maxLabel = tissueImageData.max()

start = time()


background = 1
backgroundVoxels = (tissueImageData == background)
backgroundVoxelsDil = scipy.ndimage.morphology.binary_dilation(backgroundVoxels, iterations = surfaceVoxelsDilationIteration)
externalVoxelsCharacteristic = (backgroundVoxelsDil - backgroundVoxels)
externalVoxels = tissueImageData[externalVoxelsCharacteristic]


L1LabelsSet = set(np.unique(externalVoxels))
L1LabelsSet.update( set(np.unique(tissueImageData[..., -1])) )
L1LabelsSet.remove(background) 
L1Labels = list(L1LabelsSet)
L1Characteristic = np.empty((maxLabel + 1, ), bool)
L1Characteristic.fill(False)
L1Characteristic[L1Labels] = True 
L1CellsVoxels = L1Characteristic[tissueImageData]

L1CellsVoxelsCopy = np.copy(L1CellsVoxels)

#L1Voxels = np.asarray(tissueImageData)
#L1Voxels.fill(1)
#L1Voxels[L1CellsVoxels] = tissueImageDataCopy[L1CellsVoxels]
#imsave("L1new.tiff", SpatialImage(L1Voxels))


L1CellsVoxelsErosion = scipy.ndimage.morphology.binary_erosion(L1CellsVoxelsCopy, iterations = L1PericlinalWallErosionIteration)
#L1VoxelsErosion = np.asarray(tissueImageData)
#L1VoxelsErosion.fill(1)
#L1VoxelsErosion[L1CellsVoxelsErosion] = tissueImageData[L1CellsVoxelsErosion]
pinImageL1CellsErosion = np.zeros(pinImage.shape, pinImage.dtype)
pinImageL1CellsErosion[L1CellsVoxelsErosion] = pinImage[L1CellsVoxelsErosion]



label2ExpressionValuesOnCellWall = np.zeros(maxLabel + 1)
label2ExpressionValues = np.zeros(maxLabel + 1)
labelVoxelCount = np.zeros(maxLabel + 1) 
cellBorderVoxelNb = np.zeros(maxLabel + 1)

label2ExpressionValuesOnAnticlinalWalls = np.zeros(maxLabel + 1)
anticlinalVoxelNb = np.zeros(maxLabel + 1)

label2ExpressionValuesOnPeriticlinalWalls = np.zeros(maxLabel + 1)
periclinalVoxelNb = np.zeros(maxLabel + 1)

label2ExpressionValuesOnPeriticlinalWallsExcludingSurface = np.zeros(maxLabel + 1)
periclinalVoxelNbExcludingSurface = np.zeros(maxLabel + 1)

for i in xrange(tissueImageData.shape[0]):
    print i
    for j in xrange(tissueImageData.shape[1]):
        for k in xrange(tissueImageData.shape[2]):
            label2ExpressionValues[tissueImageData[i, j, k]] += pinImage[i, j, k]            
            if cellWalls[i, j, k]:
                label2ExpressionValuesOnCellWall[tissueImageData[i, j, k]] += pinImage[i, j, k]            
                cellBorderVoxelNb[tissueImageData[i, j, k]] += 1
                if L1CellsVoxelsErosion[i, j, k]:
#                    label2ExpressionValuesOnAnticlinalWalls[tissueImageData[i, j, k]] += pinImageL1CellsErosion[i, j, k]
                    label2ExpressionValuesOnAnticlinalWalls[tissueImageData[i, j, k]] += pinImage[i, j, k]
                    anticlinalVoxelNb[tissueImageData[i, j, k]] += 1
                elif L1CellsVoxels[i, j, k]:
                    periclinalVoxelNb[tissueImageData[i, j, k]] += 1
                    label2ExpressionValuesOnPeriticlinalWalls[tissueImageData[i, j, k]] += pinImage[i, j, k]
                    if not externalVoxelsCharacteristic[i, j, k]:
                        label2ExpressionValuesOnPeriticlinalWallsExcludingSurface[tissueImageData[i, j, k]] += pinImage[i, j, k]
                        periclinalVoxelNbExcludingSurface[tissueImageData[i, j, k]] += 1
                    

cellVoxelNb = np.bincount(tissueImageData.flatten())

fobj = file(label2ExpressionValuesFile, "w")
cPickle.dump([label2ExpressionValuesOnCellWall, cellBorderVoxelNb, label2ExpressionValues, cellVoxelNb, label2ExpressionValuesOnAnticlinalWalls, anticlinalVoxelNb, label2ExpressionValuesOnPeriticlinalWalls, periclinalVoxelNb, label2ExpressionValuesOnPeriticlinalWallsExcludingSurface, periclinalVoxelNbExcludingSurface], fobj)
fobj.close()

print label2ExpressionValuesFile, width, hour, hmin

print "Elapsed time = ", time() - start