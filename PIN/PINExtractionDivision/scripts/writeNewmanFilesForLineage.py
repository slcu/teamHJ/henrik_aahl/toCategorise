#reads in lineage tree in format
#t0 m01 m02 m03 ...
#t1 m11 m12 m13 ...
#t2 m21 m22 m23 ...

#then from corresponding time files reads in corresponding variable 
#t0 v01 v02 v03 ...
#t1 v11 v12 v13 ...
#t2 v21 v22 v23 ...

import numpy as np

lineage = open("lineage48hprimordia.txt", "r")

dataFolder = "./results_variables/"
fileName = "organismFormatPINDATA_timePoint_"
tag = "h.txt"

lineageArr = []

for l in lineage.readlines():
#read in time and load in files accordingly
    l = l.split(" ")
    while "" in l: l.remove("")
    l[-1] = l[-1][:-1] #remove /n final character
    l = [int(i) for i in l]
    lineageArr.append(l)

lineage.close()  

for t in range(0,10):
    #read in file 
    time = lineageArr[t][0];
    print time
    dataFile = open(dataFolder + fileName + str(time) + tag, "r")
    data = dataFile.readlines()
    dataFile.close()

    #collect all data into array of proper types 
    dataCellNum = len(data) - 1 
    dataArr = []
    for i in range(1, dataCellNum  + 1):
        d = data[i].split(" ")	
        while "" in d: d.remove("")	
        d[-1] = d[-1][:-1] #remove /n final character
        newd = [float(num_string) for num_string in d] #convert all numbers to floats
        newd[0] = int(newd[0]) # cell label
        for j in range(6, 7 + int(d[6])): # approp. floats to integers
           newd[j] = int(newd[j])   
        for j in range(7 + 2*int(d[6]), 7 + 3*int(d[6])): 
           newd[j] = int(newd[j])  
        #select variables to be recorded
        dataArr.append([newd[0], newd[1], newd[2], newd[3], newd[4], newd[5]])

    # add variable = flag if cell is in lineage tree 
    print lineageArr[t][1:]
    for j in range(0, dataCellNum):
        if dataArr[j][0] in lineageArr[t][1:]:
            dataArr[j].append(1)
            print dataArr[j][0] 
        else:
            dataArr[j].append(0)             

    #write organism file
    output = open("newmanPrimordia" + str(time) + tag,"w")
    output.write("%i\n" % (1))
    output.write("%i %i %i\n" % (len(dataArr), 6, 0))	# organism format: num. cells, num. variables, timepoint
    for i in range(0, len(dataArr)):
        output.write("%f %f %f %f %i %i\n" % (dataArr[i][3], dataArr[i][4], dataArr[i][5], dataArr[i][2], dataArr[i][6], dataArr[i][0]))
    output.close()





