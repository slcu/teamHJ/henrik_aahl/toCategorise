from openalea.image.algo.graph_from_image import SpatialImageAnalysis
from openalea.image.all import imread, imsave, SpatialImage
from openalea.image.all import imread, imsave
from itertools import product
from time import time
from extractTop import *
from os.path import isfile, join
from os import listdir
import scipy.ndimage 
import numpy as np
import os
import cPickle
import scipy




segmentedImageFileName = "/media/yassin/Seagate Expansion Drive/storage/data/Lisa/LisaImages/7Oct2013/0hrs/backGroundCorrected/0hrs_plant_1-acylYFP_hmin_3_clean_3_bk_ok.tif" 
cellWallsFileName =                    "/media/yassin/Seagate Expansion Drive/storage/data/Lisa/LisaImages/7Oct2013/0hrs/walls/0hrs_plant_1-acylYFP_hmin_3_clean_3_bk_ok_walls_width_2.npy"
signalFileName =                       "/media/yassin/Seagate Expansion Drive/storage/data/Lisa/LisaImages/7Oct2013/0hrs/tifs/0hrs_plant_1-PIN.tif"
topDataFileName = "/media/yassin/Seagate Expansion Drive/storage/data/Lisa/LisaImages/7Oct2013/0hrs/topData/0hrs_plant_1-acylYFP_hmin_3_clean_3_bk_ok_top.pkl"
                                                                                



def quantifySignalONWall(tissueImageData, cellWalls, pinImage, cid1, cid2):
    cid1Image = np.empty(tissueImageData.shape, bool)
    cid1Image.fill(False)
    cid2Image = np.empty(tissueImageData.shape, bool)
    cid2Image.fill(False)
    cid1Image[np.where(tissueImageData == cid1)] = True
    np.where(cid1Image == True)[0].shape
    np.where(tissueImageData == cid1)[0].shape
    cid2Image[np.where(tissueImageData == cid2)] = True
    np.where(tissueImageData == cid2)[0].shape
    np.where(cid2Image== True)[0].shape
    cid1ImageDil = scipy.ndimage.morphology.binary_dilation(cid1Image, iterations = 1)
    cid2ImageDil = scipy.ndimage.morphology.binary_dilation(cid2Image, iterations = 1)
    wallImage = (cid1ImageDil & cid1ImageDil) | (cid2ImageDil & cid2ImageDil)
    wallcid1cid2 = wallImage & cid1Image
    wallcid2cid1 = wallImage & cid2Image    
    pin1to2 = np.sum(np.array(pinImage[wallcid1cid2 == True]))
    pin2to1 = np.sum(np.array(pinImage[wallcid2cid1 == True]))
    return pin1to2, pin2to1


# print quantifySignalONWall(tissueImageData, cellWalls, pinImage, cid1 = 698, cid2 = 1048)



def writePinDataFile(pinPolarityFilename, segmentedImageFileName, cellWallsFileName, signalFileName):
    cellWalls = np.load(cellWallsFileName)
    pinImage = imread(signalFileName)
    tissueImageData = imread(segmentedImageFileName)
    fobj = file(topDataFileName)
    topData = cPickle.load(fobj)
    fobj.close()
    fobj = file(pinPolarityFilename, "w")
    counter = 0
    for t in topData[0]["wall_surface"].keys():
        counter += 1
        cid1 = t[0]
        cid2 = t[1]
        values = quantifySignalONWall(tissueImageData, cellWalls, pinImage, cid1, cid2)
        line = str(cid1) + " " + str(cid2) + " " + str(values[0]) + "\n"
        fobj.write(line)
        line = str(cid2) + " " + str(cid1) + " " + str(values[1]) + "\n"
        fobj.write(line)

    fobj.close()
    
if __name__ == "__main__":
    writePinDataFile("pinWithPolarity.txt", segmentedImageFileName, cellWallsFileName, signalFileName)
