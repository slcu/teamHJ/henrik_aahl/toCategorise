
timePoint = 56

fileName = "/home/Niklas/Desktop/Lisa_PIN1_analysis_July_1014/plant13/topOk/%dhrs_plant13-acylYFP_slicesOk_hmin_2_topOk_clean_3.tif"%timePoint
PINFileName = "/home/Niklas/Desktop/Lisa_PIN1_analysis_July_1014/plant13/PINTifs/%dhrs_plant13-PIN_SlicesOK.tif"%timePoint
resolution = (0.2635765, 0.2635765, 0.26)
organismFormatPINDataFileName = "organismFormatPINDATA_timePoint_%dh.txt"%timePoint
maxVolume = 10000

__doc__ = """
maxVolume is the maximum authorised volume of a cell, if volume of a cell is greater than maxVolume it will be ignored as a segmentation error.

organism format file name has the below format:

- for the first line:
numberOfCells  numberOfVariables

- for the other lines:
cellLabel cellVolume cellVolume ^ 1/3 cellCenterX cellCenterY cellCenterZ numberOfNeighbors labelsOfNeighbor_1 ... cellSurfaceArea_1 ... numberOfVoxels_1 ... totalPINOnWall_1 ...

cellVolume = micron^3

- background is labelled by 1 in the segmentation file
 
"""
