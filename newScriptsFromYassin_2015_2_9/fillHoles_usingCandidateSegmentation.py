from openalea.image.all import imread, imsave
import numpy as np
from time import time


t1 = time()
fname =    "/home/lisa/Desktop/2015_02_16_NPA_PGMYCR_timecourse/plant1_final_for_segmentation/0hrs/0hrs_plant1_trim-acylYFP_hmin_2_asf_1_s_2.00.tif"
can_didateImageName_0 = "/home/lisa/Desktop/2015_02_16_NPA_PGMYCR_timecourse/plant1_final_for_segmentation/0hrs/0hrs_plant1_trim-acylYFP_hmin_2_asf_0_s_1.50.tif"
# can_didateImageName_1 = "/media/YassinSLCU/storage/data/Lisa/plant13/segmented/4hrs_plant13-acylYFP_trim_rep_hmin_2_asf_1_s_1.00.tif"
secondCandidate = False # if can_didateImageName_1 is used 


main = imread(fname)
print main.shape
can_0 = imread(can_didateImageName_0)
# can_1 = imread(can_didateImageName_1)

newShape = (main.shape[0], main.shape[1], main.shape[2])
# newShape = (main.shape[0], main.shape[1], 125)


mainImageMaxLabel = np.max(main)
bk = (main == 1)


for z in xrange(newShape[2]):
    print z
    for x in xrange(newShape[0]):
        print z, x
        can_0CellId = can_0[x, 0, z]
        for y in xrange(newShape[1]):
            if main[x, y, z] == 1 and can_0[x, y, z] != 1:
                if (can_0CellId != can_0[x, y, z]):
                    can_0CellId = can_0[x, y, z]
                w0 = (can_0 == can_0CellId)
                mainImageMaxLabel += 1
                main[w0 & bk] = mainImageMaxLabel
            elif secondCandidate and main[x, y, z] == 1 and can_1[x, y, z] != 1:
                can_1CellId = can_1[x, y, z]
                w1 = (can_1 == can_1CellId)
                mainImageMaxLabel += 1
                main[w1 & bk] = mainImageMaxLabel
                

imsave(fname[:-4] + "_fh.tif", main)
print "Elpased time = ", time() - t1

 




