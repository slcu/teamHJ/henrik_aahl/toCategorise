import numpy as np
from scipy.ndimage.morphology import binary_dilation
from openalea.image.algo.graph_from_image import SpatialImageAnalysis


def extractL1L2(image, backgroundLabel, maxLabel, checkCellsOnZLast = True):
    backgroundLabelVoxels = (image == backgroundLabel)
    backgroundLabelVoxelsDil = binary_dilation(backgroundLabelVoxels, iterations = 1)
    externalVoxels = image[backgroundLabelVoxelsDil - backgroundLabelVoxels]
    L1LabelsSet = set(np.unique(externalVoxels))
    if checkCellsOnZLast:
        L1LabelsSet.update( set(np.unique(image[..., -1])) )
    L1LabelsSet.remove(backgroundLabel) 
    L1Labels = list(L1LabelsSet)
    
    
    L1Characteristic = np.empty((maxLabel + 1, ), bool)
    L1Characteristic.fill(False)
    L1Characteristic[L1Labels] = True 
    L1CellsVoxels = L1Characteristic[image]        
    L2LabelsSet = set(np.unique(image[binary_dilation(L1CellsVoxels, iterations = 1) - L1CellsVoxels]))
    L2LabelsSet.remove(backgroundLabel)        
    L2Labels = list(L2LabelsSet)
    
    
    
    return L1Labels, L2Labels