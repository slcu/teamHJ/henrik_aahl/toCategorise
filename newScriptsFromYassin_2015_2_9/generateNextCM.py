import numpy as np
import cPickle

fobj = file("/media/YassinSLCU/storage/data/Lisa/plant13/ALT/t0_t4/matchingScoresListNew_0h_to_4h.pkl")
(matching, scoresList) = cPickle.load(fobj)
fobj.close()

print matching
print scoresList



divisions = dict()

for (d, m) in matching:
    divisions.setdefault(m, []).append(d)

counter = 0
for m, dList in divisions.iteritems():
    print m, dList
    if len(dList) > 1:
        counter += 1
        
print counter


fobj = file("colorMap.pkl")
cmap = cPickle.load(fobj)
fobj.close()

print type(cmap)
newColorMap = np.array(cmap)

for m, dList in divisions.iteritems():
    for d in dList:
        newColorMap[d] = cmap[m]
        
fobj = file("nextCM_t4.pkl", "w")
cPickle.dump(newColorMap, fobj)
fobj.close()
