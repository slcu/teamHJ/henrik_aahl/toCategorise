


pathApplyTrsf = "/home/yassin/devCode/vt/build/bin/applyTrsf"
pathBlockmatching = "/home/yassin/devCode/vt/build/bin/blockmatching"
ALTParameter = 3.0
matchingNamePostfix = "0h_to_4h"


stretchingFactor = 1.0

savePath = "/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/ALT_0_4hrs/"
t0Path = "/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/0hrs_plant13-acylYFP_trim_rep.tif"
t1Path = "/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/4hrs_plant13-acylYFP_trim_rep.tif"

t0Resolution = (0.2635765, 0.2635765, 0.26)
t1Resolution = (0.2635765, 0.2635765, 0.26 / stretchingFactor)


seg0Path = "/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/0hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_hf_clean_3_selectedCellsRemoved_labelsOK.tif"
seg1Path = "/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/4hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_hf_clean_3_selectedCellsRemoved_labelsOK.tif"


seg0Resolution = (0.2635765, 0.2635765, 0.26)
seg1Resolution = (0.2635765, 0.2635765, 0.26 / stretchingFactor)


