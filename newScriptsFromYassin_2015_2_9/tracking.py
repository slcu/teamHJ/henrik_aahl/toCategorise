import os
import sys
import numpy as np
from openalea.image.serial.basics import imread, imsave
from vplants.mars_alt.alt.candidate_lineaging import candidate_lineaging
from openalea.image.all import SpatialImage, SpatialImageAnalysis
from vplants.mars_alt.alt.optimal_lineage import optimal_lineage
from scipy import ndimage as nd
import cPickle
from makeLabelsConsecutive import consecutiveLabels
from parameters import pathApplyTrsf, pathBlockmatching, ALTParameter, t0Path, t1Path, t0Resolution, t1Resolution, seg0Path, seg1Path, seg0Resolution, seg1Resolution, savePath, matchingNamePostfix 
import time


startTime = time.time()

t0=imread(t0Path)
t1=imread(t1Path)
t0.resolution= t0Resolution
t1.resolution= t1Resolution
t0seg = imread(seg0Path)
t1seg = imread(seg1Path)
t0seg.resolution = seg0Resolution
t1seg.resolution = seg1Resolution

imsave('%st0.inr'%savePath, t0)
imsave('%st1.inr'%savePath, t1)


os.system(pathBlockmatching +
          " -ref %st1.inr"%savePath +
          " -flo %st0.inr"%savePath +
          " -res %st0_affine_registered.inr"%savePath +
          " -res-trsf %saffine_full_trsf"%savePath +
          " -trsf-type affine" +
          " -estimator wlts" +
          " -pyramid-highest-level 5" +
          " -pyramid-lowest-level 2" +
          " -lts-fraction 0.55")



os.system(pathBlockmatching +
          " -ref %st1.inr"%savePath +
          " -flo %st0.inr"%savePath +
          " -init-trsf %saffine_full_trsf"%savePath +
          " -res %st0_NL_registered.inr"%savePath +
          " -res-trsf %sNL_trsf.inr"%savePath +
          " -trsf-type vectorfield" +
          " -estimator wlts" +
          " -py-gf" +
          " -pyramid-highest-level 5" +
          " -pyramid-lowest-level 2" +
          " -elastic-sigma %f %f %f"%(ALTParameter, ALTParameter, ALTParameter) +
          " -fluid-sigma %f %f %f"%(ALTParameter, ALTParameter, ALTParameter))






t0seg.resolution=t0.resolution
imsave('%st0Seg.inr'%savePath, t0seg)

os.system(pathApplyTrsf +
          " %st0Seg.inr"%savePath +
          " %sseg_t0_on_t1.inr"%savePath +
          " -trsf %sNL_trsf.inr"%savePath + 
          " -nearest") # should use linear for intensity images



seg_t0_on_t1 = imread("%sseg_t0_on_t1.inr"%savePath)
t1_seg = imread(seg1Path)
seg_t0_on_t1[seg_t0_on_t1 == 0 ] = 1

im0, dict0 =  consecutiveLabels(seg_t0_on_t1)
seg_t0_on_t1 = SpatialImage(im0) # we need labels from 1 to n with no gap between labels and 1 as the background

im1, dict1 = consecutiveLabels(t1_seg)
t1_seg = SpatialImage(im1) 

seg_t0_on_t1.resolution = t1_seg.resolution = t1.resolution

imsave("%st0_on_t1_seg_homogenise.tif"%savePath, seg_t0_on_t1)
imsave("%st1_seg_homogenise.tif"%savePath, t1_seg)

fobj = file("%sconsecutiveLabelsDicts.pkl"%savePath, "w")
cPickle.dump((dict0, dict1), fobj)
fobj.close()


#============================================================================================

# --- ROMAIN'S METHOD
# candy = candidate_lineaging(seg_t0_on_t1, t1_seg, dist = 3, ndiv = 2, bkgdLabel = 1)
# max_t0=seg_t0_on_t1.max()
# max_t1=t1_seg.max()
# 
# # - computing of the lineage
# final_lineage=optimal_lineage(max_t0, max_t1, candidates=candy, ndiv = 2, optimization_method = "basic_solve")
# 
# fobj = file("romanLineages.pkl", "w")
# cPickle.dump(final_lineage, fobj)
# fobj.close()
#============================================================================================



max_t0 = seg_t0_on_t1.max()
max_t1 = t1_seg.max()


an1 = SpatialImageAnalysis(seg_t0_on_t1)
an2 = SpatialImageAnalysis(t1_seg)
volumes_2 = dict(zip(an2.labels(), an2.volume()))
boundingboxes1 = an1.boundingbox()
labels_bb = dict(zip(an1.labels(), boundingboxes1))
matrix = np.zeros((max_t0 + 1, max_t1 + 1), dtype=np.float32)

for cell, bb in labels_bb.iteritems():
    image_rec=t1_seg[bb][seg_t0_on_t1[bb]==cell]
    vol=np.sum(np.ones_like(image_rec))
    labels=np.unique(image_rec)
    recouv={}
    for lab in labels:
        if lab!=1:
            recouv[lab]=2*nd.sum(image_rec==lab)/np.float32(vol+volumes_2[lab])
        else :
            recouv[lab]=nd.sum(image_rec==lab)/np.float32(vol)
    tab_for_graph=[]
    for cell_recouv in recouv:
        matrix[cell, cell_recouv]=recouv[cell_recouv]

# TO DOUBLE CHECK


#matching=zip(range(2, max_t1), np.argmax(matrix, axis=1)+2) # chaque fille -> une mere

if dict0 != None:
    t0Ont1ConcatTot0Dict = dict((j, i) for i, j in dict0)
else:
    t0Ont1ConcatTot0Dict = dict((i, i) for i in xrange(np.max(seg_t0_on_t1) + 1))

if dict1 != None:
    t1ConcatTot1Dict = dict((j, i) for i, j in dict1)
else:
    t1ConcatTot1Dict = dict((i, i) for i in xrange(np.max(t1_seg) + 1))
    




matching = []
scoresList = []
for d in xrange(2, max_t1 + 1):
    score=np.max(matrix[:, d])
    matching.append((t1ConcatTot1Dict[d], t0Ont1ConcatTot0Dict[np.argmax(matrix[:, d])] ))
    scoresList.append(score)

fobj = file("%smatchingScoresListNew_%s.pkl"%(savePath, matchingNamePostfix), "w")
cPickle.dump((matching, scoresList), fobj)
fobj.close()

print "Elapsed time = ", time.time() - startTime


# matching = []
# scoresList = []
# for d in xrange(2, max_t1 + 1):
#     score=np.max(matrix[:, d])
#     matching.append((d, np.argmax(matrix[:, d])))
#     scoresList.append(score)
# 
# fobj = file("matchingScoresList_%s.pkl"%matchingNamePostfix, "w")
# pickle.dump((matching, scoresList), fobj)
# fobj.close()



