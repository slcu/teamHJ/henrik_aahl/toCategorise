from openalea.image.algo.graph_from_image import SpatialImageAnalysis
from openalea.image.all import imread, imsave, SpatialImage
from time import time
import numpy as np
from os import listdir
from os.path import isfile, join



def removeTopCells(im, background = 1):
    zTop = set(np.unique(im[..., -1]))
#     zBottom = set(np.unique(im[..., 0]))
#     xTop = set(np.unique(im[-1, ...]))
#     xBottom = set(np.unique(im[0, ...]))
#     yTop = set(np.unique(im[:, -1, :]))
#     yBottom = set(np.unique(im[:, 0, :]))
    
    borderCells = set()
    borderCells.update(zTop)
#     borderCells.update(yTop)
#     borderCells.update(xTop)
#     borderCells.update(zBottom)
#     borderCells.update(yBottom)
#     borderCells.update(xBottom)
#     borderCells.remove(background)
#     
    print len(borderCells)
    
    if len(borderCells) < 10:
        counter = 0    
        for i in borderCells:
            print "%2.2f"%( (counter / float(len(borderCells))) * 100) + " % is done, wait please ..."
            np.putmask(im, im == i, background)
            counter += 1
    else:    
        s = im.shape
        for i in xrange(s[0]):
            print i
            for j in xrange(s[1]):
                for k in xrange(s[2]):
                    if im[i, j, k] in borderCells:
                        im[i, j, k] = background
    return im