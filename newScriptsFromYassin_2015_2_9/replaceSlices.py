from openalea.image.serial.basics import imread, imsave, SpatialImage
import numpy as np

#plant 13. 2013.10.07
#slicesToRemove = [["0hrs", [48]], ["4hrs", [10, 31, 33,41,42,44]], ["8hrs", [4,5,7,10,12,13,37]], ["12hrs", [13, 15,16,17,19,33,34,35,36,37,38,39,63,64,77,85]], ["16hrs", [4,9,10,12,13,19,20,21,23,24]], ["20hrs",[4,9,10,17,19,22,26,27,28,32,33,37,38,40,41,43,48,70,74,78,80,83,90,98,100,102,107,112,116,118,122,123,125,127,129,131]], ["24hrs", [34,35,36,38,39,41,42,43,44,46,55,140,146]], ["28hrs", [8, 10, 12,13,78,80,81,82,87,91,92,93,94]], ["32hrs", [2,4,6,8,55,57,59,61,63,65,67,69,71,73]], ["36hrs", [26,27,28,30,47,48,49,51,53,54,56,58,60,103,105,107,142,144,147]], ["40hrs", []],  ["44hrs", [38, 69, 147, 149]], ["48hrs", [80,82,88,158,209,211,213,215,217,219,221,223,224,226,229,269,271,273]], ["52hrs", [120, 122, 125, 131, 161, 165, 169, 172, 221, 228, 231, 235, 240, 277]], ["56hrs", [223, 225,228, 230, 232, 236, 240, 245, 248, 254, 258, 260, 333, 334, 339, 346, 418]], ["60hrs", [5, 8, 39, 95, 98, 177, 180, 182, 185, 189, 204,275, 277, 279, 283]], ["64hrs", [188, 192, 198, 207, 227, 232]], ["68hrs", [22,24,28,31,42]], ["72hrs", [37,40]], ["76hrs", [34,35,46,59,154,156,158,160]], ["80hrs", [170,171,174]]]
slicesToRemove =  [["64hrs", [4,208]]]


def findSliceNbToReplace(toReplace):
    slices = []
    for i in toReplace:
        dist = 1
        while ((i - dist) in toReplace) and ((i + dist) in toReplace):
            dist += 1
        print dist
        if (i - dist) not in toReplace:
            slices.append(i - dist)
            print "here"
        else:
            slices.append(i + dist)
            print "here1"
    print slices
    return slices


path = "/home/lisa/Desktop/2015_02_16_NPA_PGMYCR_timecourse/64hrs/stackreg/"


for item in slicesToRemove:
    fname = path + item[0]+"_plant6-clv3.tif"
    image = imread(fname)
    newImage = image[:, :, :]
    slices = findSliceNbToReplace(item[1])
    print "item = ", item[1]
    print slices
    print "----------------------"
    for sn in xrange(len(item[1])):
        print sn, item[1][sn] - 1, slices[sn] -1
        newImage[..., item[1][sn] - 1] = image[..., slices[sn] - 1]
    imsave(fname[:-4] + "_rep.tif", newImage)
       


