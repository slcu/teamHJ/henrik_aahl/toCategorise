from openalea.image.algo.graph_from_image import SpatialImageAnalysis
from openalea.image.all import imread, imsave, SpatialImage
from time import time
import numpy as np
from os import listdir
from os.path import isfile, join

#fobj = file("0hrs_plant_1-acylYFP_hmin_2.npy", "w")
#np.save(fobj, im0)
#fobj.close()
#
#exit()

#imS0 = SpatialImage(im0, background = 1)
#imS1 = SpatialImage(im1, background = 1)
#
#labels0 = np.unique(im0)
#labels1 = np.unique(im1)
#
#sImA0 = SpatialImageAnalysis(imS0)
#sImA1 = SpatialImageAnalysis(imS1)
#
#vols0 = sImA0.volume(labels0, real = True)
#vols1 = sImA1.volume(labels1, real = True)
#
#print labels0.shape, labels1.shape
#print len(set(labels0))


def removeBorderCells(fName, resolution, background = 1):
    im = imread(fName)
    zTop = set(np.unique(im[..., -1]))
    zBottom = set(np.unique(im[..., 0]))
    xTop = set(np.unique(im[-1, ...]))
    xBottom = set(np.unique(im[0, ...]))
    yTop = set(np.unique(im[:, -1, :]))
    yBottom = set(np.unique(im[:, 0, :]))
    
    borderCells = set()
    borderCells.update(zTop)
    borderCells.update(yTop)
    borderCells.update(xTop)
    borderCells.update(zBottom)
    borderCells.update(yBottom)
    borderCells.update(xBottom)
    
    print len(borderCells)
    
    counter = 0
    for i in borderCells:
        print "%2.2f"%( (counter / float(len(borderCells))) * 100) + " % is done, wait please ..."
        np.putmask(im, im == i, background)
        counter += 1
    imsave(im, fName[:-4] + "_borderCellsRemoved.tif")
    

def removeBorderCellsNew(fName, resolution, background = 1):
    im = imread(fName)
    zTop = set(np.unique(im[..., -1]))
    zBottom = set(np.unique(im[..., 0]))
    xTop = set(np.unique(im[-1, ...]))
    xBottom = set(np.unique(im[0, ...]))
    yTop = set(np.unique(im[:, -1, :]))
    yBottom = set(np.unique(im[:, 0, :]))
    
    borderCells = set()
    borderCells.update(zTop)
    borderCells.update(yTop)
    borderCells.update(xTop)
    borderCells.update(zBottom)
    borderCells.update(yBottom)
    borderCells.update(xBottom)
    borderCells.remove(background)
    
    print len(borderCells)
    
    s = im.shape
    print s
    for i in xrange(s[0]):
        print i
        for j in xrange(s[1]):
            for k in xrange(s[2]):
                if im[i, j, k] in borderCells:
                    im[i, j, k] = background
    
    imsave(fName[:-4] + "_borderCellsRemoved.tif", im)
    
    
def borderCellsLabels(fName, background = 1):
    im = imread(fName)
    zTop = set(np.unique(im[..., -1]))
    zBottom = set(np.unique(im[..., 0]))
    xTop = set(np.unique(im[-1, ...]))
    xBottom = set(np.unique(im[0, ...]))
    yTop = set(np.unique(im[:, -1, :]))
    yBottom = set(np.unique(im[:, 0, :]))
    
    borderCells = set()
    borderCells.update(zTop)
    borderCells.update(yTop)
    borderCells.update(xTop)
    borderCells.update(zBottom)
    borderCells.update(yBottom)
    borderCells.update(xBottom)
    borderCells.remove(background)
    
    print len(borderCells)
    return borderCells
    


def removeTopCells(fName, resolution, background = 1):
    im = imread(fName)
    zTop = set(np.unique(im[..., -1]))
#     zBottom = set(np.unique(im[..., 0]))
#     xTop = set(np.unique(im[-1, ...]))
#     xBottom = set(np.unique(im[0, ...]))
#     yTop = set(np.unique(im[:, -1, :]))
#     yBottom = set(np.unique(im[:, 0, :]))
    
    borderCells = set()
    borderCells.update(zTop)
#     borderCells.update(yTop)
#     borderCells.update(xTop)
#     borderCells.update(zBottom)
#     borderCells.update(yBottom)
#     borderCells.update(xBottom)
#     borderCells.remove(background)
#     
    print len(borderCells)
    
    s = im.shape
    print s
    for i in xrange(s[0]):
        print i
        for j in xrange(s[1]):
            for k in xrange(s[2]):
                if im[i, j, k] in borderCells:
                    im[i, j, k] = background
    
    imsave(fName[:-4] + "_topCellsRemoved.tif", im)
    

    
def removeBorderCellsNPArray(fName, resolution, background = 1):
    im = np.load(fName)
    zTop = set(np.unique(im[..., -1]))
    zBottom = set(np.unique(im[..., 0]))
    xTop = set(np.unique(im[-1, ...]))
    xBottom = set(np.unique(im[0, ...]))
    yTop = set(np.unique(im[:, -1, :]))
    yBottom = set(np.unique(im[:, 0, :]))
    
    borderCells = set()
    borderCells.update(zTop)
    borderCells.update(yTop)
    borderCells.update(xTop)
    borderCells.update(zBottom)
    borderCells.update(yBottom)
    borderCells.update(xBottom)
    
    print len(borderCells)
    
    counter = 0
    for i in borderCells:
        print "%2.2f"%( (counter / float(len(borderCells))) * 100) + " % is done, wait please ..."
        np.putmask(im, im == i, background)
        counter += 1
    fobj = file(fName[:-4] + "_borderCellsRemoved.npy", "w")
    np.save(fobj, im)
    
    

def removeSelectedCells(fName, cellsToRemove, resolution, background = 1):
    im = imread(fName)

    
    counter = 0
    for i in cellsToRemove:
        print "%2.2f"%( (counter / float(len(cellsToRemove))) * 100) + " % is done, wait please ..."
        np.putmask(im, im == i, background)
        counter += 1
    imsave(fName[:-4] + "_selectedCellsRemoved.tif", im)


if __name__ == "__main__":
    
#     removeBorderCellsNew("/media/Seagate Backup Plus Drive/storage/data/Lisa/plant13/acylYFP_plant13/ALT/test/segmentations/4hrs_plant13-acylYFP_slReplaced_hmin_2_asf_3_s_0.00_hf_clean_4.tif", (0.2635765, 0.2635765, 0.26), 1)
#     myPath = "folder where you have all the segmented files"
#     onlyfiles = [ f for f in listdir(myPath) if isfile(join(myPath,f)) ]
#     for fName in onlyfiles:
#         removeBorderCellsNew(join(myPath,fName), resolution = (1, 1 ,1))
    
#     removeTopCells("/media/Seagate Backup Plus Drive/storage/workspace/newLisa/new/newtest/PlantA_zoomIn_4_slow-acylYFP_ed_hmin_1_asf_2_sigma_2.00.tif", resolution = (0.2441960, 0.2441960, 0.4))
#     cellsToRemove = [36, 1713, 1689, 1700, 1707, 1708, 1705, 1645, 1718, 1715, 1717, 1716, 1696, 1709, 1704, 1714, 1684, 1622, 1550, 838, 1695, 1710, 872, 1383, 1473, 1319, 1711, 1837, 1882, 1873, 1845, 1850, 1875,  1849, 1875, 1864, 1853, 1882, 1526, 1873]
#     cellsToRemove = [1748, 1667, 1748, 1832, 1831, 1756, 1857, 1759, 1885, 1892, 1901, 1907, 1905, 1903, 1904, 1909, 1912, 1911, 1913, 1896, 1898, 1897, 1894, 1895, 1899, 1906, 1900, 1902, 1908, 1910, 1914, 1526, 1804, 1803, 1794, 1830, 1882, 1873, 1875, 1828, 1837, 1850, 1880, 1878, 1864, 1868, 1893, 1849, 1845, 1847, 1858, 1845, 1874, 1879, 1891, 1872, 1915, 1853, 1863, 1834, 1633, 1591, ]
#     cellsToRemove = [2172, 2175, 2177, 2178, 2179, 2180, 2181, 2182, 2183, 2184, 2185, 2186, 2187, 2188, 2189, 2190, 2176, 2174, 2125, 2146, 2081, 2091, 1896, 2165, 2171, 2169, 2170, 2173, 2145, 2165, 2066, 1870, 1874, 1726, 1844, 1874, 1845, 1940, 1909, 1953, 1863, 1974, 1945, 1873, 1805, 1715, 1694, 1873, 1779, 1826, 1805, 1675, 1571, 1466, 1488, 1886, 1855, 1814, 1734, 1678, ]
    cellsToRemove = [2452, 2450, 2449, 2448, 2456, 2451, 2458, 2465, 2464, 2465, 2467]
#     4250 3716    7   89
    fName = "/media/YassinSLCU/storage/data/Lisa/plant13/segmentedCleanedSelected/4hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.00_hf_new_hf_hf_clean_3_selectedCellsRemoved_labelsOK.tif"
    removeSelectedCells(fName, cellsToRemove, resolution = (0.2415320, 0.2415320, 0.24))
#     removeBorderCellsNew("/media/Seagate Backup Plus Drive/storage/data/Yassin/chapter5Project/images/plantC/P1/segmented/plantC_P1_0h_slicesRemoved_SR_hmin_1_asf_0_s_2.00_clean_4_selectedCellsRemoved.tif", resolution = (1, 1, 1))
    
    
