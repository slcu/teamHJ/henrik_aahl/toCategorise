pathApplyTrsf = "/home/yassin/devCode/vt/build/bin/applyTrsf"
pathBlockmatching = "/home/yassin/devCode/vt/build/bin/blockmatching"


ALTParameter = 5.0

#paths and files
rootPath = "/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/"

timesList = ["48", "52"]

t0t1PathList = [
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/48hrs_plant13-acylYFP_trim_rep.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/52hrs_plant13-acylYFP_trim_rep.tif"
]

seg01PathList = [
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/48hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_hf_clean_3.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/52hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_selectedCellsRemoved_hf_clean_3.tif"
]

###resolutions
stretchingFactor = 1.0

t0Resolution = (0.2635765, 0.2635765, 0.26)
t1Resolution = (0.2635765, 0.2635765, 0.26 / stretchingFactor)

seg0Resolution = (0.2635765, 0.2635765, 0.26)
seg1Resolution = (0.2635765, 0.2635765, 0.26 / stretchingFactor)

#long lists of files
"""
#timesList = ["0", "4", "8", "12", "16", "20", "24", "28", "32", "36", "40", "44", "48", "52", "56", "60", "64", "68", "72", "76", "80"]
timesList = [ "48", "52", "56", "60", "64", "68", "72", "76", "80"]

t0t1PathList = [
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/0hrs_plant13-acylYFP_trim_rep.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/4hrs_plant13-acylYFP_trim_rep.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/8hrs_plant13-acylYFP_trim_rep.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/12hrs_plant13-acylYFP_trim_rep.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/16hrs_plant13-acylYFP_trim_rep.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/20hrs_plant13-acylYFP_trim_rep.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/24hrs_plant13-acylYFP_trim_rep.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/28hrs_plant13-acylYFP_trim_rep.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/32hrs_plant13-acylYFP_trim_rep.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/36hrs_plant13-acylYFP_trim_rep.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/40hrs_plant13-acylYFP_trim_rep.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/44hrs_plant13-acylYFP_trim_rep.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/48hrs_plant13-acylYFP_trim_rep.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/52hrs_plant13-acylYFP_trim_rep.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/56hrs_plant13-acylYFP_trim_rep.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/60hrs_plant13-acylYFP_trim_rep.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/64hrs_plant13-acylYFP_trim_rep.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/68hrs_plant13-acylYFP_trim_rep.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/72hrs_plant13-acylYFP_trim_rep.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/76hrs_plant13-acylYFP_trim_rep.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/intensity_images/80hrs_plant13-acylYFP_trim_rep.tif"
]

seg01PathList = [
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/0hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_hf_clean_3_selectedCellsRemoved_labelsOK.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/4hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_hf_clean_3_selectedCellsRemoved_labelsOK.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/8hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_hf_clean_3.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/12hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_new_clean_3_selectedCellsRemoved_labelsOK.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/16hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_new_clean_3_selectedCellsRemoved_hf_clean_3_selectedCellsRemoved_selectedCellsRemoved_hf_clean_3.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/20hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_hf_clean_3_selectedCellsRemoved_labelsOK.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/24hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_clean_3.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/28hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_clean_3_selectedCellsRemoved_hf_selectedCellsRemoved_clean_3_hf_clean_3_selectedCellsRemoved_labelsOK.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/32hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_new_2_clean_3_selectedCellsRemoved_clean_3.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/36hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_new_2_test_clean_3_selectedCellsRemoved_hf_clean_3.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/40hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_clean_3_selectedCellsRemoved_labelsOK.tif",
#"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/44hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_clean_3_selectedCellsRemoved_hf_clean_3.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/48hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_hf_clean_3.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/52hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_selectedCellsRemoved_hf_clean_3.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/56hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_clean_3_selectedCellsRemoved_clean_3.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/60hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_new_2_test_hf_clean_3_selectedCellsRemoved_labelsOK.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/64hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_new_3_hf_clean_3_selectedCellsRemoved_labelsOK.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/68hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_1.5_hf_clean_3.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/72hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_1.5_clean_3.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/76hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_1.5_hf_clean_3.tif",
"/home/lisa/Desktop/timecourse_anaylsis/segmentations_ALT_2013_10_07_PGMY/plant13Segmentations/final/cleaned_3/80hrs_plant13-acylYFP_trim_rep_hmin_2_asf_3_s_0.25_fh_1.2_clean_3.tif"
]
"""


