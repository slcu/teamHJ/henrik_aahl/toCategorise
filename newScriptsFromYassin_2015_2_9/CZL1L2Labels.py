from openalea.image.algo.graph_from_image import SpatialImageAnalysis
from openalea.image.all import imread, imsave, SpatialImage
import scipy.ndimage as nd
import numpy as np
from cellTopology import extractL1L2

backgroundLabel = 1

def extractCellsInTheCenterOld(imageFileName, radius = 50):
    
    tissueImage = imread(imageFileName)
    spImAnalysis = SpatialImageAnalysis(tissueImage)
    shape = tissueImage.shape
    print shape
    L1Labels, L2Labels = extractL1L2(tissueImage, 1, maxLabel = np.max(tissueImage), checkCellsOnZLast = True)
    found = False
    labels = set()
    zDepth = 0
    for z in xrange(shape[2] - 1, -1, -1):
        for x in xrange(100, shape[0] - 100):
            for y in xrange(100, shape[1] - 100):
                if tissueImage[x, y, z] in L1Labels:# or tissueImage[x, y, z] in L2Labels:
                    labels.add(tissueImage[x, y, z])
                    found = True
                                        
        if found and zDepth > 10:
            break
        elif found:
#             print "zDepth", zDepth
            zDepth += 1
    labels = list(labels)
    centers = spImAnalysis.center_of_mass(labels, real = False)
    if len(labels) == 1:
        centers = {labels[0]: centers}
    labelsDistanceToCenter = []
    distMin = 100000
    selectedLabelMinDist = -1
    for lab, c in centers.iteritems():
        if np.sqrt(((c[0] - (shape[0]/ 2.0)) ** 2) + ((c[1] - (shape[1]/ 2.0))**2)) < distMin:
            distMin = np.sqrt(((c[0] - (shape[0]/ 2.0)) ** 2) + ((c[1] - (shape[1]/ 2.0))**2))
            selectedLabelMinDist = lab
            
#     print "distMin", distMin, selectedLabelMinDist
    selectedLabelMinDistCenter = centers[selectedLabelMinDist]
    L1Centers = spImAnalysis.center_of_mass(L1Labels, real = False)
    L2Centers = spImAnalysis.center_of_mass(L2Labels, real = False)
    cellsInRaduis = list([selectedLabelMinDist])
    for id, c in L1Centers.iteritems():
        if (c[0] - selectedLabelMinDistCenter[0]) ** 2 + (c[1] - selectedLabelMinDistCenter[1]) ** 2 < (radius ** 2):
            cellsInRaduis.append(id)
    for id, c in L2Centers.iteritems():
        if (c[0] - selectedLabelMinDistCenter[0]) ** 2 + (c[1] - selectedLabelMinDistCenter[1]) ** 2 < (radius ** 2):
            cellsInRaduis.append(id)
    return cellsInRaduis


from openalea.image.algo.graph_from_image import SpatialImageAnalysis
from openalea.image.all import imread, imsave, SpatialImage
import scipy.ndimage as nd
import numpy as np
from cellTopology import extractL1L2

backgroundLabel = 1

def extractCellsInTheCenter(imageFileName, radius = 50):
    
    tissueImage = imread(imageFileName)
    spImAnalysis = SpatialImageAnalysis(tissueImage)
    shape = tissueImage.shape
    print shape
    L1Labels, L2Labels = extractL1L2(tissueImage, 1, maxLabel = np.max(tissueImage), checkCellsOnZLast = True)
    found = False
    labels = set()
    zDepth = 0
    for z in xrange(shape[2] - 1, -1, -1):
        for x in xrange(50, shape[0] - 50):
            for y in xrange(50, shape[1] - 50):
                if tissueImage[x, y, z] in L1Labels:# or tissueImage[x, y, z] in L2Labels:
                    labels.add(tissueImage[x, y, z])
                    found = True
                                        
        if found and zDepth > 10:
            break
        elif found:
#             print "zDepth", zDepth
            zDepth += 1
    labels = list(labels)
    print labels
    return labels
    centers = spImAnalysis.center_of_mass(labels, real = False)
    if len(labels) == 1:
        centers = {labels[0]: centers}
    labelsDistanceToCenter = []
    distMin = 100000
    selectedLabelMinDist = -1
    for lab, c in centers.iteritems():
        if np.sqrt(((c[0] - (shape[0]/ 2.0)) ** 2) + ((c[1] - (shape[1]/ 2.0))**2)) < distMin:
            distMin = np.sqrt(((c[0] - (shape[0]/ 2.0)) ** 2) + ((c[1] - (shape[1]/ 2.0))**2))
            selectedLabelMinDist = lab
            
    print "distMin", distMin, selectedLabelMinDist
    selectedLabelMinDistCenter = centers[selectedLabelMinDist]
    L1Centers = spImAnalysis.center_of_mass(L1Labels, real = False)
    L2Centers = spImAnalysis.center_of_mass(L2Labels, real = False)
    cellsInRaduis = list([selectedLabelMinDist])
    for id, c in L1Centers.iteritems():
        if (c[0] - selectedLabelMinDistCenter[0]) ** 2 + (c[1] - selectedLabelMinDistCenter[1]) ** 2 < (radius ** 2):
            cellsInRaduis.append(id)
#     for id, c in L2Centers.iteritems():
#         if (c[0] - selectedLabelMinDistCenter[0]) ** 2 + (c[1] - selectedLabelMinDistCenter[1]) ** 2 < (radius ** 2):
#             cellsInRaduis.append(id)
    return cellsInRaduis


def extractCellsInTheCenterLabelGiven(imageFileName, givenCellLabel, radius = 50):
    
    tissueImage = imread(imageFileName)
    spImAnalysis = SpatialImageAnalysis(tissueImage)
    shape = tissueImage.shape
    print shape
    L1Labels, L2Labels = extractL1L2(tissueImage, 1, maxLabel = np.max(tissueImage), checkCellsOnZLast = True)
    found = False
    labels = [givenCellLabel]
    centers = spImAnalysis.center_of_mass([labels[0]], real = False)
    print centers
    L1Centers = spImAnalysis.center_of_mass(L1Labels, real = False)
    L2Centers = spImAnalysis.center_of_mass(L2Labels, real = False)
    print L1Centers
    cellsInRaduis = list(labels)
    for id, c in L1Centers.iteritems():
        if (c[0] - centers[0]) ** 2 + (c[1] - centers[1]) ** 2 < (radius ** 2):
            cellsInRaduis.append(id)
    for id, c in L2Centers.iteritems():
        if (c[0] - centers[0]) ** 2 + (c[1] - centers[1]) ** 2 < (radius ** 2):
            cellsInRaduis.append(id)
    
    return cellsInRaduis
    
    
    
    

if __name__ == "__main__":
    imageFileName = "/media/YassinSLCU/storage/data/Yassin/chapter5Project/done/plantC_P1/plantC_P1_0h_slicesRemoved_SR_hmin_1_asf_0_s_2.00_clean_4_selectedCellsRemoved_borderCellsRemoved_selectedCellsRemoved_clean_4.tif"
    L1L2SelectedCells = extractCellsInTheCenter(imageFileName, radius = 50)
    vols = np.bincount(imread(imageFileName).flatten())
    selectedVols = vols[L1L2SelectedCells]
    print "L1L2SelectedCells = ", L1L2SelectedCells
    print "L1L2CZ averageVols = ", np.average(selectedVols)        
                







