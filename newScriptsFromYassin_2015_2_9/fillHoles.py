from openalea.image.all import imread, imsave
import numpy as np



fname =   "/home/lisa/Desktop/2015_02_16_NPA_PGMYCR_timecourse/plant1_final_for_segmentation/0hrs/0hrs_plant1_trim-acylYFP_hmin_2_asf_1_s_2.00_fh.tif"

candidateImageName = "/home/lisa/Desktop/2015_02_16_NPA_PGMYCR_timecourse/plant1_final_for_segmentation/0hrs/0hrs_plant1_trim-acylYFP_hmin_2_asf_0_s_1.50.tif"

main = imread(fname)

can = imread(candidateImageName)



holeCenters = [[242, 258, 96], [238, 288, 96], [282, 278, 96], [275, 254, 96]]
#[257, 77, 105] 
#[233, 119, 116] 
#[220, 98, 175] 
#[96, 100, 243]

for holeCenter in holeCenters:

    print main[holeCenter[0], holeCenter[1], holeCenter[2]], can[holeCenter[0], holeCenter[1], holeCenter[2]]
    raw_input()
    
    if main[holeCenter[0], holeCenter[1], holeCenter[2]]== 1 and can[holeCenter[0], holeCenter[1], holeCenter[2]] != 1:
        w = np.where(can == can[holeCenter[0], holeCenter[1], holeCenter[2]])
        mainImageMaxLabel = np.max(main)
        mainImageMaxLabel += 1

        for i in xrange(len(w[0])):
            if main[w[0][i], w[1][i], w[2][i]] == 1:
                main[w[0][i], w[1][i], w[2][i]] = mainImageMaxLabel
        
imsave(fname[:-4] + "_hf.tif", main)
        
  

 




