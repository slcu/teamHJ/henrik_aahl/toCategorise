from openalea.image.algo.graph_from_image import SpatialImageAnalysis
from openalea.image.all import imread, imsave, SpatialImage
from openalea.image.all import imread, imsave
from itertools import product
from os.path import isfile, join
from os import listdir
import scipy.ndimage 
import numpy as np
import os
import cPickle
import scipy

################################
from parameters_multi_2015_02_16_plant1 import rootPath, workspacePath, timesList, radius, finalSegmentationPathList  
###############################

timePoint = 0

#to image extracted walls against intensity image, in ImageJ --> Adjust brightness and contrast. Merge with intensity image cyan and extracted walls yellow. Look at orthogonal views and reslices.

def calculateWalls(fileName, wallWidth = 2):    
    tissueImageData = np.asfortranarray(imread(fileName))    
    imageLaplace = scipy.ndimage.laplace(tissueImageData)
    walls = np.empty(tissueImageData.shape, dtype = bool)
    walls.fill(True)
    walls[imageLaplace == 0] = False 
    if wallWidth > 2:
        walls = scipy.ndimage.morphology.binary_dilation(walls, iterations =  wallWidth / 2 - 2).astype(walls.dtype)
    np.save(fileName[:-4] + "_walls", walls)
    wallsNew = np.empty(tissueImageData.shape, dtype = np.uint8)
    wallsNew.fill(0)
    wallsNew[walls] = 255
    imsave(fileName[:-4] + "_walls.tif", SpatialImage(wallsNew))

if __name__ == "__main__":

    for i in range(0, len(finalSegmentationPathList)):
        fName = finalSegmentationPathList[i]
        calculateWalls(fName)

