############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@slcu.cam.ac.uk>
#
############################################################################

import time
import random
import cPickle
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import vtk
from scipy.ndimage.morphology import binary_dilation
from openalea.image.algo.graph_from_image import SpatialImageAnalysis
from openalea.image.serial.basics import imread, imsave
from cellTopology import extractL1L2

def rangeCorrespondance(a, b, c, d, x):
    return ((d - c) / (b - a)) * (x - a) + c

class Parameters(object):
    wallsOpacity = 0.03    # The opacity of the cell walls
    backgroundOpacity = 0.0     # The opacity of background which is usually marked by 1, but can have another value. 
    colorsOpacity = 0.95    # The opacity of the colors used for showing the cells
    wallsRGB = (0, 1, 0)   # The color of the walls in RGB code
    backgroundRGB = (0, 0, 0)    # background color, is backgroundOpacity is set to zero, changing its value does not affect the way the tissue is viewed
    cmColorsNb = 128    # Different number of colors used for cells 
    cmShuffle = True    # When set to True, the generated colormap is shuffled 
    differentColoredCellClusterSize = 8    # We make sure that in a cluster of color codes which will be used later in a colormap, we have different color codes. This reduces the chances of having neighbor cells with the same color. 
    cellsToRemove = [100 + i for i in xrange(1000)] # The cells that will be removed when the key "d" is pressed.
    cellsToShow = [273, 485, 286, 356, 610, 771, 878, 529, 636, 789, 819, 578, 567, 820, 903, 500, 660, 845, 983, 853, 1009, 1008, 1123, 962, 1052, 1212, 1121, 1197, 931, 1037, 1112, 1159, 928, 980, 1129, 985, 1080, 1184, 1214, 1304, 1234, 1279, 1313, 1295, 1073, 1145, 1109, 1189, 622, 842, 677, 851, 950, 1078, 979, 1108, 1126, 1188, 1285, 1161, 1199, 1242, 1350, 1354, 1395, 704, 727, 892, 930, 1036, 907, 1282, 1196, 1253, 1292, 1319, 1358, 1343, 1310, 1348, 1135, 1182, 1230, 1241, 1321, 1200, 1208, 1293, 1301, 828, 924, 778, 872, 1012, 1035, 1051, 1118, 1119, 1147, 755, 861, 873, 915, 997, 1005, 1013, 1091, 1144, 1195, 1092, 1158, 1215, 1207, 1228, 1239, 1252, 1309, 1347, 1311, 1325, 1105, 1134, 1175, 1220, 1254, 1275, 690, 674, 817, 906, 635, 726, 818, 925, 960, 1066, 1093, 1141, 1258, 1249, 1355, 1373, 1371, 1427, 1397, 1436, 1431, 1451, 1445, 1457, 1444, 1450, 1466, 1455, 1401, 1425, 1408, 1400, 1418, 1421, 1411, 510, 587, 759, 694, 887, 1007, 889, 1014, 739, 729, 862, 738, 808, 901, 976, 987, 1043, 1068, 1180, 1179, 1246, 1240, 1274]
    cellsExpressingGenes = [273, 485, 286, 356, 610, 771, 878, 529, 636, 789, 819, 578, 567, 820, 903, 500, 660, 845, 983, 853, 1009, 1008, 1123, 962, 1052, 1212, 1121, 1197, 931, 1037, 1112, 1159, 928, 980, 1129, 985, 1080, 1184, 1214, 1304, 1234, 1279, 1313, 1295, 1073, 1145, 1109, 1189, 622, 842, 677, 851, 950, 1078, 979, 1108, 1126, 1188, 1285, 1161, 1199, 1242, 1350, 1354, 1395, 704, 727, 892, 930, 1036, 907, 1282, 1196, 1253, 1292, 1319, 1358, 1343, 1310, 1348, 1135, 1182, 1230, 1241, 1321, 1200, 1208, 1293, 1301, 828, 924, 778, 872, 1012, 1035, 1051, 1118, 1119, 1147, 755, 861, 873, 915, 997, 1005, 1013, 1091, 1144, 1195, 1092, 1158, 1215, 1207, 1228, 1239, 1252, 1309, 1347, 1311, 1325, 1105, 1134, 1175, 1220, 1254, 1275, 690, 674, 817, 906, 635, 726, 818, 925, 960, 1066, 1093, 1141, 1258, 1249, 1355, 1373, 1371, 1427, 1397, 1436, 1431, 1451, 1445, 1457, 1444, 1450, 1466, 1455, 1401, 1425, 1408, 1400, 1418, 1421, 1411, 510, 587, 759, 694, 887, 1007, 889, 1014, 739, 729, 862, 738, 808, 901, 976, 987, 1043, 1068, 1180, 1179, 1246, 1240, 1274] #
    geneExpressionValues = [133.45060221021319, 162.70101385970801, 186.34638755229435, 119.98724470567925, 160.03932498067357, 229.31365088527878, 133.45060221021319, 126.36684820939669, 125.4655355731099, 152.46266563188775, 116.24116406111226, 137.36567897408398, 114.80469704703017, 168.32013482655847, 78.456448386776884, 133.66184735934291, 200.72514070305709, 205.49928107338869, 150.0967199616349, 189.43056672958821, 178.65706412397262, 99.116223971663203, 125.80352781171744, 123.40941612158065, 148.70250197737874, 181.60041320184669, 103.11579879518584, 185.45915792594954, 102.60881043727453, 117.08614465763112, 181.34691902289103, 147.40686506271652, 114.60753490784245, 229.91922031278395, 122.62076756482971, 189.28973663016839, 65.922569538413683, 114.80469704703017, 125.85985985148537, 122.66301659465564, 130.49317012239717, 156.80023269401792, 166.96816587212831, 122.38135639581601, 134.53499397574575, 134.01392260789243, 92.201466090150461, 124.55013992688112, 129.12711815802498, 100.55269098574527, 170.75649554652119, 150.37838016047453, 168.71445910493395, 90.412923827518867, 96.93335743065613, 196.55656976023064, 132.00005218618915, 88.21597427656981, 184.36068315047498, 174.69973833027595, 152.49083165177169, 217.06143223575518, 129.81718564518206, 127.29632686556745, 100.93293225417877, 220.37093957212073, 215.65313124155708, 145.66057182991082, 162.88409298895377, 135.85879691029197, 180.12169715793868, 199.8801601065382, 127.59207007434905, 199.95057515624811, 150.33613113064857, 172.06621547112547, 116.84673348861745, 287.80039117432642, 228.46867028875988, 122.26869231628018, 89.173618952624523, 97.524843848219334, 149.87139180256321, 207.24557430619433, 210.85082485134151, 227.82085183142874, 108.12935033453113, 60.571025760460856, 92.257798129918399, 229.97555235255189, 113.14290187387641, 173.43226743549764, 193.40197553322687, 201.40112518027217, 177.20651409994858, 92.708454448061786, 184.05085693175141, 219.48370994577593, 194.47228428881743, 155.42009771970376, 149.11090926569622, 211.73805447768632, 228.41233824899197, 211.20290009989102, 149.99813889204103, 99.693627379284422, 152.54716369153962, 240.12940252072025, 149.8009767528533, 154.16670983486745, 162.44751968075235, 237.2001364527882, 127.2681608456835, 146.50555242642972, 114.80469704703017, 171.17898584478067, 238.63660346687027, 196.64106781988255, 206.72450293834103, 118.10012137345376, 158.87043515548916, 116.24116406111226, 95.032151088488675, 213.31535159118818, 160.37731721928114, 131.56347887798773, 233.31322570880141, 190.52904150506274, 148.71658498732074, 187.57160941724669, 117.14247669739905, 129.67635554576225, 162.20810851173869, 122.90242776366934, 224.62400857459906, 225.49715519100187, 116.81856746873348, 199.24642465914906, 141.70324603621415, 148.16734759958348, 144.36493491524857, 182.88196710656698, 166.6020076136368, 118.38178157229339, 179.16405248188397, 76.893234283216984, 119.50842236765189, 150.16713501134481, 160.09565702044151, 167.39065617038773, 126.80342151759811, 217.01918320592924, 92.370462209454246, 150.67412336925614, 162.82776094918583, 84.54030868171273, 70.386883690021691]    #
    cropImageShape = [(0, -1), (0, -1), (0, -1)]    #  
    geneExpressionThreshold = 5
    generateColorMapMaxCellNumber = 10000
    pickledColorMapName = "/home/yassin/workspace/imageProcessing/src/colorMap.pkl"

# fobj = file("/media/YassinSLCU/storage/data/Benoit/images/secondlabel2ExpressionValue_Width_2_erosion_1_surfaceDil_2.pkl")
# [label2ExpressionValuesOnCellWall, cellBorderVoxelNb, label2ExpressionValues, cellVoxelNb, label2ExpressionValuesOnAnticlinalWalls, anticlinalVoxelNb, label2ExpressionValuesOnPeriticlinalWalls, periclinalVoxelNb, label2ExpressionValuesOnPeriticlinalWallsExcludingSurface, periclinalVoxelNbExcludingSurface] = cPickle.load(fobj)
# fobj.close()
# 
# fobj = file("/media/YassinSLCU/storage/data/Benoit/images/label2ExpressionValue_Width_2_erosion_1_surfaceDil_2.pkl")
# [label2ExpressionValuesOnCellWall2, cellBorderVoxelNb2, label2ExpressionValues2, cellVoxelNb2, label2ExpressionValuesOnAnticlinalWalls2, anticlinalVoxelNb2, label2ExpressionValuesOnPeriticlinalWalls2, periclinalVoxelNb2, label2ExpressionValuesOnPeriticlinalWallsExcludingSurface2, periclinalVoxelNbExcludingSurface2] = cPickle.load(fobj)
# fobj.close()
# 
# Parameters.geneExpressionValues = np.nan_to_num(label2ExpressionValues2 / cellVoxelNb)
# Parameters.geneExpressionValues[1] = 0 

class viewer3D(object):
    """
    This class define a three dimension viewer for any three dimensional matrix. 
    """    
    def __init__(self, imageFileName, cellWallFileName, background=1, dataSpacing=(1, 1, 1), renBackground=(0, 0, 0), wallId=0, checkBackground=False, addCellWalls=True, geneExpressionColorMapName="jet"):
        self.startTime = time.time()
        self.imageData = np.asfortranarray(self.crop3DImage(imread(imageFileName)))
        self.originalImageData = np.copy(self.imageData)
        self.background = background
        self.maxLabel = self.imageData.max()
#         self.geneExpressionValues = np.zeros(self.maxLabel + 1)
        self.geneExpressionValues = Parameters.geneExpressionValues
        print "maxLabel = ", self.maxLabel, len(np.unique(self.imageData))        
        self.wallId = wallId
        self.geneExpressionColorMapName = geneExpressionColorMapName
        print "Elapsed time to read the image = ", time.time() - self.startTime
        self.startTime = time.time() 
        print "imageData shape =", self.imageData.shape        
        self.dataSpacing = dataSpacing
        self.imageFileName = imageFileName
        self.renderer = vtk.vtkRenderer()
        self.renderer.SetViewport([0.0, 0.0, 1.0, 1.0])
        self.renderWin = vtk.vtkRenderWindow()
        self.renderWin.SetWindowName("TissueViewer: " + self.imageFileName.split("/")[-1])
        self.renderWin.AddRenderer(self.renderer)
        self.renderInteractor = vtk.vtkRenderWindowInteractor()
        self.renderInteractor.SetRenderWindow(self.renderWin)
        self.renderInteractor.AddObserver("CharEvent", self.CharEventFunction)
        self.renderer.SetBackground(renBackground[0], renBackground[1], renBackground[2])
        self.snapShotNb = 0
        self.expressionThreshold = Parameters.geneExpressionThreshold
        self.cellWallFileName = cellWallFileName
        self.showGeneExpression = False
        self.txt = vtk.vtkTextActor()
        self.txt.SetTextScaleMode(0)
        self.txt.SetInput("TissueViewer")
        txtprop = self.txt.GetTextProperty()
        txtprop.SetFontFamilyToArial()
        txtprop.SetFontSize(14)
        txtprop.SetColor(1, 1, 1)
        self.txt.SetDisplayPosition(12, 12)
        fobj = file(Parameters.pickledColorMapName)
        self.pickledCM = cPickle.load(fobj)
        fobj.close()
        self.firstShow = True
        if checkBackground:
            self.checkBG()
        if addCellWalls:
            self.addCellwalls()

    def crop3DImage(self, image3D):
        if Parameters.cropImageShape[0][1] == -1:
            croppedImage = image3D[Parameters.cropImageShape[0][0]:, ...]
        else:
            croppedImage = image3D[Parameters.cropImageShape[0][0]:Parameters.cropImageShape[0][1], ...]
        if Parameters.cropImageShape[1][1] == -1:
            croppedImage = croppedImage[:, Parameters.cropImageShape[1][0]:, :]
        else:
            croppedImage = croppedImage[:, Parameters.cropImageShape[1][0]:Parameters.cropImageShape[1][1], :]
        if Parameters.cropImageShape[2][1] == -1:
            croppedImage = croppedImage[:, :, Parameters.cropImageShape[2][0]:]
        else:
            croppedImage = croppedImage[:, :, Parameters.cropImageShape[2][0]:Parameters.cropImageShape[2][1]]
        return croppedImage    
    
    def checkBG(self):
        print "Checking the background label ..."
        largestCellLabel = np.bincount(self.imageData.flatten()).argmax()
        if largestCellLabel != self.background:
            print "Setting background label ..."
            np.putmask(self.imageData, self.imageData == self.background, self.maxLabel + 1)
            self.maxLabel += 1   
            np.putmask(self.imageData, self.imageData == largestCellLabel, self.background)
        print "Elapsed time to check the background = ", time.time() - self.startTime
        self.startTime = time.time()            
        
    def addCellwalls(self):                        
        print "Adding cell walls ..."
        walls = self.crop3DImage(np.load(cellWallFileName))
        self.imageData[walls] = self.wallId
        print "Elapsed time to add cell walls = ", time.time() - self.startTime
        self.startTime = time.time()
    
    def colorFunctions(self, colorLabelValues, colorMapName="jet"):        
        self.alphaChannelFunc = vtk.vtkPiecewiseFunction()
        self.alphaChannelFunc.AddPoint(self.wallId, Parameters.wallsOpacity)
        self.alphaChannelFunc.AddPoint(self.background, Parameters.backgroundOpacity)
        self.alphaChannelFunc.AddPoint(colorLabelValues[0], Parameters.colorsOpacity)
        self.alphaChannelFunc.AddPoint(colorLabelValues[1], Parameters.colorsOpacity)
        self.alphaChannelFunc.AddPoint(colorLabelValues[2], Parameters.colorsOpacity)        
        self.colorFunc = vtk.vtkColorTransferFunction()        
        self.colorFunc.AddRGBPoint(self.wallId, Parameters.wallsRGB[0], Parameters.wallsRGB[1], Parameters.wallsRGB[2])
        self.colorFunc.AddRGBPoint(self.background, Parameters.backgroundRGB[0], Parameters.backgroundRGB[1], Parameters.backgroundRGB[2])
        if colorMapName == "RGB":
            self.colorFunc.AddRGBPoint(colorLabelValues[0], 1.0, 0.0, 0.0)
            self.colorFunc.AddRGBPoint(colorLabelValues[1], 0.0, 1.0, 0.0)
            self.colorFunc.AddRGBPoint(colorLabelValues[2], 0.0, 0.0, 1.0)
        else:
            normal = mpl.colors.Normalize(vmin=colorLabelValues[0], vmax=colorLabelValues[2])
            colMap = plt.get_cmap(colorMapName)
            sm = mpl.cm.ScalarMappable(norm=normal, cmap=colMap)            
            for i in xrange(colorLabelValues[0], colorLabelValues[2] + 1):
                cVals = sm.to_rgba(i)
                self.colorFunc.AddRGBPoint(i, cVals[0], cVals[1], cVals[2])
                
    def importDataCreateVolume(self, volumeData, datatype=np.uint16): 
        self.dataImporter = vtk.vtkImageImport()
        self.dataImporter.SetImportVoidPointer(volumeData, volumeData.nbytes)
        if datatype == np.uint16:
            self.dataImporter.SetDataScalarTypeToUnsignedShort()
        elif datatype == np.uint8:
            self.dataImporter.SetDataScalarTypeToUnsignedChar()            
        self.dataImporter.SetNumberOfScalarComponents(1)
        self.dataImporter.SetDataExtent(0, volumeData.shape[0] - 1, 0, volumeData.shape[1] - 1, 0, volumeData.shape[2] - 1)
        self.dataImporter.SetWholeExtent(0, volumeData.shape[0] - 1, 0, volumeData.shape[1] - 1, 0, volumeData.shape[2] - 1)
        self.dataImporter.SetDataSpacing(self.dataSpacing)   
        self.volumeProperty = vtk.vtkVolumeProperty()
        self.volumeProperty.SetColor(self.colorFunc)
        self.volumeProperty.SetScalarOpacity(self.alphaChannelFunc)         
        compositeFunction = vtk.vtkVolumeRayCastCompositeFunction()
        self.volumeMapper = vtk.vtkVolumeRayCastMapper()
        self.volumeMapper.SetVolumeRayCastFunction(compositeFunction)
        self.volumeMapper.SetInputConnection(self.dataImporter.GetOutputPort())         
        self.volume = vtk.vtkVolume()
        self.volume.SetMapper(self.volumeMapper)
        self.volume.SetProperty(self.volumeProperty)
        
    def show(self, data=None):
        if data == None:
            colorLabelValues = [self.background + 1, self.maxLabel / 2, self.maxLabel]
            self.colorFunctions(colorLabelValues)
            self.importDataCreateVolume(self.imageData)
        else:
#             self.renderer.RemoveAllViewProps()
            self.renderer.RemoveActor(self.volume)
            self.importDataCreateVolume(data, datatype = np.uint8)
        self.renderer.AddActor(self.txt)
        self.renderer.AddVolume(self.volume)
        
        if self.showGeneExpression:        
            colorMapBar = vtk.vtkScalarBarActor()
            colorMapBar.SetLookupTable(self.colorbarColorFunc)
            colorMapBar.GetLabelTextProperty().SetColor(0, 0, 1)
            coord = colorMapBar.GetPositionCoordinate()
            coord.SetCoordinateSystemToNormalizedViewport()
            coord.SetValue(0.05, 0.7)
            colorMapBar.SetWidth(0.035)
            colorMapBar.SetHeight(0.25)
            self.renderer.AddActor(colorMapBar)
        
        self.renderWin.Render()        
        if self.firstShow:
            self.firstShow = False
            self.renderInteractor.Start()
        
    def CharEventFunction(self, obj, ev):
        keypressed = self.renderInteractor.GetKeyCode()
        if keypressed == 'c':
            self.txt.SetInput("Colormap is changing ...")
            self.renderWin.Render()
            self.changeColormap()
            self.txt.SetInput("TissueViewer")
            self.renderWin.Render()
        if keypressed == 's':
            self.snapshot()
        if keypressed == 'x':
            self.extractCellData()
        if keypressed == 'd':
            self.removeSelectedCells()
        if keypressed == 'l':
            self.removeL1Cells()
        if keypressed == 'k':
            self.showSelectedLabels(Parameters.cellsToShow)
        if keypressed == 'g':
            self.showGeneExpression = True
            self.showSelectedCells(self.selectCells())
        if keypressed == 't':
            self.renderer.RemoveActor(self.txt)
            self.renderWin.Render()
        if keypressed == "w":
            self.generatePickleColormap(Parameters.generateColorMapMaxCellNumber)
        if keypressed == "h":
            print " Press \"s\" to take an snapshot, \n \"c\" to change the colormap, \n \"l\" to remove L1 cells, \n \"x\" to extract cell data, \n \"d\" to remove selected cells, \n \"k\" to show selected cells, \n \"g\" to show gene expression patterns, \n \"t\" to remove text from the viewer window"
            

    def snapshot(self):
        w2i = vtk.vtkWindowToImageFilter()
        w2i.SetInput(self.renderWin)
        png = vtk.vtkPNGWriter()
        png.SetInput(w2i.GetOutput())
        snapshotNbStr = str(self.snapShotNb)
        snapshotNbStr = (5 - len(snapshotNbStr)) * "0" + snapshotNbStr
        png.SetFileName("./snapshot/snapshot_%s.png" % snapshotNbStr)
        png.Write()
        self.snapShotNb += 1
        
    def makeNewColormap(self):
        colorBasketNb = Parameters.cmColorsNb / Parameters.differentColoredCellClusterSize
        colormapBase = np.array([], dtype=np.uint8, order="F")
        blockColorIndex = np.array([i * Parameters.differentColoredCellClusterSize + 2 for i in xrange(colorBasketNb)], dtype=np.uint8, order='F')
        for j in xrange(Parameters.differentColoredCellClusterSize):
            newBlockColorIndex = blockColorIndex + j
            if Parameters.cmShuffle:
                random.shuffle(newBlockColorIndex)
            colormapBase = np.concatenate([colormapBase, newBlockColorIndex])
        colormap = np.array(colormapBase, dtype=np.uint8, order='F')
        for i in xrange(int(np.ceil(self.maxLabel / (Parameters.cmColorsNb))) + 1):
            colormap = np.hstack([colormap, colormapBase])
        self.colormap = np.hstack([np.array([0, 1], dtype=np.uint8), colormap])
        
        
    def generatePickleColormap(self, maxCellNumber):
        colorBasketNb = Parameters.cmColorsNb / Parameters.differentColoredCellClusterSize
        colormapBase = np.array([], dtype=np.uint8, order="F")
        blockColorIndex = np.array([i * Parameters.differentColoredCellClusterSize + 2 for i in xrange(colorBasketNb)], dtype=np.uint8, order='F')
        for j in xrange(Parameters.differentColoredCellClusterSize):
            newBlockColorIndex = blockColorIndex + j
            if Parameters.cmShuffle:
                random.shuffle(newBlockColorIndex)
            colormapBase = np.concatenate([colormapBase, newBlockColorIndex])
        colormap = np.array(colormapBase, dtype=np.uint8, order='F')
        for i in xrange(int(np.ceil(maxCellNumber / (Parameters.cmColorsNb))) + 1):
            colormap = np.hstack([colormap, colormapBase])
        newColormap = np.hstack([np.array([0, 1], dtype=np.uint8), colormap])
        fobj = file("colorMap.pkl", "w")
        cPickle.dump(newColormap, fobj)
        fobj.close()


    def selectCells(self):
        self.colorbarColorFunc = vtk.vtkColorTransferFunction()
        norm = mpl.colors.Normalize(vmin=self.geneExpressionValues.min(), vmax=self.geneExpressionValues.max())
        print self.geneExpressionValues.min(), self.geneExpressionValues.max()
        cmap = plt.get_cmap(self.geneExpressionColorMapName)
        m = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)            
        for i in np.linspace(self.expressionThreshold, self.geneExpressionValues.max(), Parameters.cmColorsNb):
            cVals = m.to_rgba(i)
            self.colorbarColorFunc.AddRGBPoint(i, cVals[0], cVals[1], cVals[2])
        return np.where(self.geneExpressionValues >= self.expressionThreshold)[0]

    def showSelectedCells(self, selectedCellsList):        
        print "Displaying selected cells ..." 
        self.txt.SetInput("Displaying the selected cells ...")
        self.renderWin.Render()
        colorLabelValues = [self.background + 1, Parameters.cmColorsNb / 2 , Parameters.cmColorsNb]
        self.colorFunctions(colorLabelValues, self.geneExpressionColorMapName)
        self.colormap = np.empty((self.maxLabel + 1,), dtype=np.uint8)
        self.colormap.fill(self.background)
        self.colormap[selectedCellsList] = np.uint8(rangeCorrespondance(self.expressionThreshold, self.geneExpressionValues.max(), colorLabelValues[0], colorLabelValues[2], self.geneExpressionValues[selectedCellsList]))
        self.colormap[0] = 0
        self.colorIndexImage = np.array(self.colormap[self.imageData], order='F')  # Attention: data must be np.uint8
        self.txt.SetInput("TissueViewer")
        self.show(self.colorIndexImage)
    
    def showSelectedLabels(self, labels):
        self.txt.SetInput("Isolating the selected cells ...")
        self.renderWin.Render()
        try:
            allLabelsSet = set(np.unique(self.originalImageData))
            labels2Remove = list(allLabelsSet - set(labels))
            self.colormap[labels2Remove] = self.background
            self.colorIndexImage = np.array(self.colormap[self.imageData], order='F')  # Attention: data must be np.uint8
            self.txt.SetInput("TissueViewer")
            self.show(self.colorIndexImage)
            print "Added"
        except AttributeError:
            print "First you have to apply a colormap, press 'c'"
            self.txt.SetInput("First you have to apply a colormap, press 'c'")
            self.renderWin.Render()
    
                      
    def changeColormap(self):
        print "Wait while the new color map is being applied ..."
        self.colormap = self.pickledCM[:self.maxLabel + 1]
#         self.makeNewColormap(Parameters.cmColorsNb)        
        colorLabelValues = [self.background + 1, Parameters.cmColorsNb / 2, Parameters.cmColorsNb]         
        self.colorFunctions(colorLabelValues)
        self.volumeProperty.SetColor(self.colorFunc)
        self.colorIndexImage = np.array(self.colormap[self.imageData], order='F')  # Attention: data must be np.uint8
        self.show(self.colorIndexImage)
        
    def removeSelectedCells(self):
        print "removing"
        self.txt.SetInput("Removing cells ...")
        self.renderWin.Render()
        try:
#             selectedCellsList = self.L1Cells()
            selectedCellsList = Parameters.cellsToRemove
            self.colormap[selectedCellsList] = self.background
            self.colorIndexImage = np.array(self.colormap[self.imageData], order='F')  # Attention: data must be np.uint8
            self.txt.SetInput("TissueViewer")
            self.show(self.colorIndexImage)
        except AttributeError:
            print "First you have to apply a colormap, press 'c'"
            self.txt.SetInput("First you have to apply a colormap, press 'c'")
            self.renderWin.Render()

    def removeL1Cells(self):
        print "removing"
        self.txt.SetInput("Removing cells ...")
        self.renderWin.Render()
        try:
            selectedCellsList = self.L1Cells()
#             selectedCellsList = Parameters.cellsToRemove
            self.colormap[selectedCellsList] = self.background
            self.colorIndexImage = np.array(self.colormap[self.imageData], order='F')  # Attention: data must be np.uint8
            self.txt.SetInput("TissueViewer")
            self.show(self.colorIndexImage)
        except AttributeError:
            print "First you have to apply a colormap, press 'c'"
            self.txt.SetInput("First you have to apply a colormap, press 'c'")
            self.renderWin.Render()
    

    def L1Cells(self, checkCellsOnZLast = True):
        L1Labels, L2Labels = extractL1L2(self.originalImageData, self.background, self.maxLabel, checkCellsOnZLast)
        return L1Labels
        
    def extractCellData(self):
        labels = np.unique(self.originalImageData)
        start = time.time()
        backgroundVoxels = (self.originalImageData == self.background)
        backgroundVoxelsDil = binary_dilation(backgroundVoxels, iterations = 1)
        externalVoxels = self.originalImageData[backgroundVoxelsDil - backgroundVoxels]
        L1CellsLabels = set(np.unique(externalVoxels))
        print L1CellsLabels
        print "Elapsed time = ", time.time() - start
        imAnalysis = SpatialImageAnalysis(self.originalImageData)        
#        if self.background in labels : del labels[labels.index(self.background)]
        
        neighborhood = imAnalysis.neighbors(labels) # neighborhood is a dictionary of cell labels and the list of corresponding neighbors i.e. {cellLable: [NeighborCellLable_1, ...], ..} 
        print "neighborhood is extracted"
        edges = []
        for source, targets in neighborhood.iteritems():
            if source in labelset:
                for target in targets:
                    if source < target and target in labelset:
                        edges.append((source, target))
        edgesDict = dict((source, []) for source, target in edges)
        for source, target in edges:
            edgesDict[source].append(target)
        print "edges extracted"    
        barycenter = dict( zip( labels, imAnalysis.center_of_mass(labels, real = True) ) )
        print "barycenter extracted"
        volumes = dict( zip( labels, imAnalysis.volume(labels, real = True) ) )
        print "volumes extracted"
        wall_surface = imAnalysis.wall_surfaces(edgesDict, real = True)

if __name__ == "__main__":
    
    imageFileName = "/home/lisa/Desktop/2015_02_16_NPA_PGMYCR_timecourse/plant1_final_for_segmentation/76hrs/76hrs_plant1_trim-acylYFP_hmin_2_asf_1_s_2.00_clean_3.tif"
    cellWallFileName = "/home/lisa/Desktop/2015_02_16_NPA_PGMYCR_timecourse/plant1_final_for_segmentation/76hrs/76hrs_plant1_trim-acylYFP_hmin_2_asf_1_s_2.00_clean_3_walls.npy"

    viewer = viewer3D(imageFileName, cellWallFileName, geneExpressionColorMapName="Greens", dataSpacing=(1.0, 1.0, 1.0), background=1, wallId=0, checkBackground=True, addCellWalls=False)
    viewer.show()
    
    
                
