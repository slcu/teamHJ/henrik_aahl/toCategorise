import cPickle
from numpy import array
from time import time, sleep
import numpy as np
from openalea.image.all import imread
from openalea.image.algo.graph_from_image import SpatialImageAnalysis
from parameters_multi_2015_02_16_plant1 import rootPath, timesList, finalSegmentationPathList

#from tiffFileRes import getTiffFileResolution

"""
This module contains a class with methods to extract the data needed to simulate a gene network model from a set of segmented images 
"""

class ReadAndcPickle(object):
    """
    Read a list of images, extract and cPickle volumes, barycenters of cells, contact areas between cells, adjacency graphs, L1 layer, and cells on the stem.  
    """
    
    def __init__(self, fileNames, cPickleFileName, background = 1):
        self.fileNames = fileNames
        self.cPickleFileName = cPickleFileName
        self.realZStemCriterionList = []
        for fname in fileNames:
            print "File name = ", fname
 #           self.realZStemCriterionList.append(float(raw_input("Enter z coordinate below which the cells are on the stem:")))       
            self.realZStemCriterionList.append(float(0)) 
        self.background = background
        
    def getResolution(self, fname):
        print "Enter voxel sizes for the image ", fname
        xVSize = float(raw_input("Enter voxel size X: "))
        yVSize = float(raw_input("Enter Y voxel size Y: "))
        zVSize = float(raw_input("Enter Z voxel size Z: "))
        return [xVSize, yVSize, zVSize]
    
    def readImages(self, imageResolution):
        """
        Read images using imread from openalea.image package
        
        :Returns:
        -    self.readImages    - List of read images 
        """
        self.readIms = [imread(fName) for fName in self.fileNames]
#        self.resolutions = [self.getResolution(fName) for fName in self.fileNames]
        self.resolutions = [imageResolution for fName in self.fileNames]
        for i in xrange(len(self.readIms)):
            print self.resolutions[i]
            self.readIms[i].resolution = self.resolutions[i] 
        return self.readIms
    
    def analyze(self, imageResolution):
        """
        Extract property graphs from read images
        
        :Returns:
        -    self.pGraphs    -    List of extracted property graphs
        """
        self.readImages(imageResolution)
        self.imAnalysis = [SpatialImageAnalysis(im) for im in self.readIms]
        return self.imAnalysis
    
    def extractFromAnalysis(self):
        """
        Read extracted property graphs from self.pGraphs and extract a dictionary of volumes, barycenters, contact areas, adjacency graphs, L1 layer, and cells on the stem.
        
        :Returns:
        
        -    self.topData    -    List of dictionaries of volumes, barycenters, contact areas, adjacency graphs, L1 layer, cells on the stem
        """
        self.topData = []
        for counter, im_analysis in enumerate(self.imAnalysis):
            labels = list(im_analysis.labels())
#            if self.background in labels : del labels[labels.index(self.background)]
            assert self.background in labels
            labelset = set(labels)
            neighborhood = im_analysis.neighbors(labels) # neighborhood is a dictionary of cell labels and the list of corresponding neighbors i.e. {cellLable: [NeighborCellLable_1, ...], ..} 
            print "neighborhood is extracted"
            edges = []
            for source, targets in neighborhood.iteritems():
                if source in labelset:
                    for target in targets:
                        if source < target and target in labelset:
                            edges.append((source, target))
            edgesDict = dict((source, []) for source, target in edges)
            for source, target in edges:
                edgesDict[source].append(target)
            print "edges extracted"    
            barycenters = dict(zip(labels,im_analysis.center_of_mass(labels, real = True)))
            print type(barycenters)
            print "barycenter extracted"
            volumes = dict( zip( labels, im_analysis.volume(labels, real = True) ) )
            print "volumes extracted"
            wall_surface = im_analysis.wall_surfaces(edgesDict, real = True)
            print "wall surface extracted"
            background_neighbors = set(im_analysis.neighbors(self.background))
            background_neighbors.intersection_update(labelset)            
#            stem = list(np.unique(self.readIms[0][..., : 10]))
#            print len(stem)  
            L1 = [lable for lable in background_neighbors if barycenters[lable][2]  >= self.realZStemCriterionList[counter] ]
#            L1 = [lable for lable in background_neighbors if lable not in stem]
            print "L1 length: ", len(L1)  
            stem = [lable for lable in labels if barycenters[lable][2] < self.realZStemCriterionList[counter] ]
            self.topData.append({"labels": labels, "volumes" : volumes, "barycenter" : barycenters, "L1": L1, "wall_surface": wall_surface, "stem": stem, "background_neighbors":background_neighbors, "neigbourhood": neighborhood})
        return self.topData
    
    def cPickleTop(self):
        """
        Dump the list of dictionaries  
        """
        fobj = file(self.cPickleFileName, "wb")
        cPickle.dump(self.topData, fobj)
        fobj.close()
    
    def extract(self, imageResolution):
        self.analyze(imageResolution)
        self.extractFromAnalysis()
        self.cPickleTop()
        
if __name__ == "__main__":
    for i in range(0, len(finalSegmentationPathList)):
        segmentedImage = finalSegmentationPathList[i]
        print segmentedImage
        cPickleFileName = segmentedImage[:-4]  + "_ok_top.pkl"
        RPObj = ReadAndcPickle([segmentedImage], cPickleFileName)
        resolution = (timesList[i][2][0], timesList[i][2][1], timesList[i][2][2]/timesList[i][3]) #do stretch correction here to extract correct wall_surfaces
        RPObj.extract(resolution)
