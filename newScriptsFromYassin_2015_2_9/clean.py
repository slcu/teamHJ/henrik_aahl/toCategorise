from scipy.ndimage.filters import gaussian_filter
import matplotlib.pyplot as plt
from scipy import ndimage
import scipy.ndimage as nd
from openalea.image.algo.graph_from_image import SpatialImageAnalysis
from openalea.image.all import imread, imsave, SpatialImage
from itertools import product
from os.path import isfile, join
from os import listdir
import scipy.ndimage 
import numpy as np
import os
import cPickle
import scipy
from time import time
#################
from parameters_multi_2015_02_16_plant15 import rootPath, workspacePath, timesList, radius, finalSegmentationPathList  

####################

pathfilter="/home/yassin/devCode/vt_clean/vt_copie/libvp/bin/linux/cellfilter "
pathskiz="/home/yassin/devCode/vt_clean/vt_copie/vt-exec/bin/linux/skiz "

def applyPalette(im, base_dict, threshold=None, dec=10000):
    outliers=set(np.array(base_dict.keys())[np.array(base_dict.values())>threshold])
    palette=np.zeros(np.max(im)+1, dtype=np.uint16)
    for i in outliers:
        palette[i]=base_dict[i]*dec
    return palette[im] 

def concatLabels(im):
    if (set(np.unique(im))!=set(range(1, np.max(im)+1))):
        im[im==0]=im.max()+1
        histo=nd.histogram(im, min=0, max=np.max(im), bins=np.max(im)+1)
        if np.argsort(histo)[-1]!=1:
            im[im==1]=im.max()+1
            im[im==np.argsort(histo)[-1]]=1
        labels=np.unique(im)
        labels.sort()
        labels_con=np.linspace(1, len(labels), len(labels)).astype(np.uint16)
        mapping=dict(zip(labels, labels_con))
        return applyPalette(im, mapping, dec=1), zip(labels, labels_con)
    else:
        return im, None

for i in range(0, len(finalSegmentationPathList)):
    cleanImageName = finalSegmentationPathList[i][:-4] + "_clean_%s.tif"%(radius)
    imsave("%s/segmented.inr.gz"%workspacePath, imread(finalSegmentationPathList[i]))
    path_input = "%s/segmented.inr.gz"%workspacePath
    path_output = "%s/segmented_corrected.inr.gz"%workspacePath
    os.system(pathfilter + " " + path_input + " %s/cellfilter.inr.gz -ouv -radius "%workspacePath + radius + " -chamfer")
    os.system(pathskiz + " %s/cellfilter.inr.gz "%workspacePath + path_output)
    # imsave(cleanImageName, imread(path_output))

    cleanImage = imread(path_output)
   
    # newImage = imread(cleanImageName)
    newImage, d = concatLabels(cleanImage)
    imsave(cleanImageName, SpatialImage(newImage))
