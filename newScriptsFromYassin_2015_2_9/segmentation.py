from libtiff import *
from openalea.image.algo.analysis import SpatialImageAnalysis
from openalea.image.algo.basic import logicalnot
from openalea.image.all import imread, imsave
from openalea.image.spatial_image import SpatialImage
from scipy import ndimage
from time import time
from vplants.asclepios.vt_exec.connexe import hysteresis, connected_components
from vplants.asclepios.vt_exec.morpho import dilation, erosion
from vplants.asclepios.vt_exec.recfilters import recfilters
from vplants.asclepios.vt_exec.regionalmax import regionalmax
from vplants.asclepios.vt_exec.watershed import watershed
from vplants.mars_alt.mars import segmentation
from vplants.mars_alt.mars.all import automatic_linear_parameters, \
    automatic_non_linear_parameters, fuse_reconstruction, reconstruct, \
    reconstruction_task, surface_landmark_matching_parameters
from vplants.mars_alt.mars.reconstruction import im2surface, surface2im
import numpy as np
import os
import sys
# from tiffFileRes import getTiffFileResolution
from time import time
import scipy.ndimage as nd
from scipy.ndimage.filters import gaussian_filter
from removeCells import removeTopCells


# hmin = 2, asf = 0, s = 1.5 

t1 = time()

getTifRes = False
im0_path = "/home/lisa/Desktop/2015_02_16_NPA_PGMYCR_timecourse/plant1_final_for_segmentation/52hrs/"
im0Name = "/home/lisa/Desktop/2015_02_16_NPA_PGMYCR_timecourse/plant1_final_for_segmentation/52hrs/52hrs_plant1_trim_brightened-acylYFP.tif"

filtersToApply = ["asf", "g"]

h_min = 1
# alternating sequential filter
asfFilterValue = 1
sigma = 2.0
# sigma = 1.5


segImagePath = im0_path 

print segImagePath


segImName = im0Name.split("/")[-1][:-4]
segImageName = segImName + "_hmin_%d"%(h_min)

print segImName

if "asf" in filtersToApply:
    segImageName =  segImageName + "_asf_%d"%(asfFilterValue)
if "g" in filtersToApply:
    segImageName = segImageName + "_s_%1.2f"%(sigma)
    
    
segImageName += ".tif"    
     
print segImagePath + segImageName

im0 = imread(im0Name)
if getTifRes:
    originalFileResolution = getTiffFileResolution(im0_path + im0Name)
    im0.resolution = originalFileResolution

im_filtered = im0

if "g" in filtersToApply:
    im_filtered = gaussian_filter(im0, sigma)
    print im_filtered.dtype
if "asf" in filtersToApply:   
    im_filtered = segmentation.filtering(im_filtered, filter_type="asf", filter_value= asfFilterValue)
    

# imsave(im0_path + segImageName, im_filtered)

# im_filtered2= segmentation.filtering(im0, filter_value = 0.5)
        
im_tmp = logicalnot(im_filtered)

im_tmp = regionalmax(im_tmp, h_min)
im_tmp = hysteresis(im_tmp, 1, h_min, connectivity=6)
seeds = connected_components(im_tmp, 1)
seg = watershed(seeds, im_filtered)

seg.resolution = (0.2396150, 0.2396150, 0.26)



def correctBG(image, background = 1):
    image = SpatialImage(image)
    volumes = np.bincount(image.flatten())
    largestCellLabel = np.argmax(volumes)
    print np.argmax(volumes), np.argsort(volumes)[:10], np.argsort(volumes)[-10:], volumes[:10]
    if np.argmax(volumes) != background:
        print largestCellLabel, np.argsort(volumes)[-10:]
        np.putmask(image, image == background, np.max(image) + 1)
        np.putmask(image, image == largestCellLabel, background)
    return image

seg = correctBG(seg)
seg = removeTopCells(seg, background = 1)



def applyPalette(im, base_dict, threshold=None, dec=10000):
    outliers=set(np.array(base_dict.keys())[np.array(base_dict.values())>threshold])
    palette=np.zeros(np.max(im)+1, dtype=np.uint16)
    for i in outliers:
        palette[i]=base_dict[i]*dec
    return palette[im] 

def concatLabels(im):
    if (set(np.unique(im))!=set(range(1, np.max(im)+1))):
        im[im==0]=im.max()+1
        histo=nd.histogram(im, min=0, max=np.max(im), bins=np.max(im)+1)
        if np.argsort(histo)[-1]!=1:
            im[im==1]=im.max()+1
            im[im==np.argsort(histo)[-1]]=1
        labels=np.unique(im)
        labels.sort()
        labels_con=np.linspace(1, len(labels), len(labels)).astype(np.uint16)
        mapping=dict(zip(labels, labels_con))
        return applyPalette(im, mapping, dec=1), zip(labels, labels_con)
    else:
        return im, None

seg, d = concatLabels(seg)



imsave(segImagePath + segImageName, SpatialImage(seg))
print segImageName

print "Elpased time: ", time() - t1
